package fit5042.assign.repository;

import java.util.List;

import javax.ejb.Remote;

import fit5042.assign.repository.entities.Contact;
import fit5042.assign.repository.entities.Staff;

@Remote
public interface StaffRepository {

	public void addStaff(Staff staff) throws Exception;
	
	public Staff searchStaffById(int id) throws Exception;
	
	public List<Staff> getAllStaffs() throws Exception;
	
	public void removeStaff(int id) throws Exception;
	
	public void editStaff(Staff staff) throws Exception;
    
	
}
