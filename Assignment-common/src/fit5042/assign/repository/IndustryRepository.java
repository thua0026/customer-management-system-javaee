package fit5042.assign.repository;

import java.util.List;

import javax.ejb.Remote;

import fit5042.assign.repository.entities.Industry;

@Remote
public interface IndustryRepository {

	public void addIndustry(Industry industry) throws Exception;

    public Industry searchIndustryById(int industryId) throws Exception;

    public List<Industry> getAllIndustries() throws Exception;

    public void removeIndustry(int industryId) throws Exception;

    public void editIndustry(Industry industry) throws Exception;
	
}
