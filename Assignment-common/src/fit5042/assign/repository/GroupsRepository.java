package fit5042.assign.repository;

import java.util.List;

import javax.ejb.Remote;

import fit5042.assign.repository.entities.Groups;

@Remote
public interface GroupsRepository {

	public void addGroups(Groups groups) throws Exception;
	
	public Groups searchGroupsById(int id) throws Exception;
	
	public List<Groups> getAllGroups() throws Exception;
	
	public void removeGroups(int id) throws Exception;
	
	public void editGroups(Groups groups) throws Exception;
	
}
