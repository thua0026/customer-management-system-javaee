package fit5042.assign.repository;

import java.util.List;
import java.util.Set;

import javax.ejb.Remote;

import fit5042.assign.repository.entities.Contact;
import fit5042.assign.repository.entities.Customer;
import fit5042.assign.repository.entities.Staff;


@Remote
public interface CustomerRepository {

    public void addCustomer(Customer customer) throws Exception;

    public Customer searchCustomerById(int customerId) throws Exception;

    public List<Customer> getAllCustomers() throws Exception;

    public void removeCustomer(int customerId) throws Exception;

    public void editCustomer(Customer customer) throws Exception;
    
//    ----------------------------------------------
    public List<Contact> getAllContacts () throws Exception;
	
	public Customer searchCustomerByContact(Contact contact) throws Exception;
	
//	public List<Customer> searchCustomersByYearAndIndustry(int yearRegistered, int industryId) throws Exception;
    
}
