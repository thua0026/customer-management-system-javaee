package fit5042.assign.repository.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;


@Entity
@NamedQueries({
    @NamedQuery(name = Customer.GET_ALL_QUERY_NAME, query = "SELECT p FROM Customer p order by p.customerId")})
public class Customer implements Serializable{
	
	public static final String GET_ALL_QUERY_NAME = "Customer.getAll";
	
	private int customerId;
	private String customerName;
	private int yearRegistered;
	private int quantityOrdered;
	private int noOfEmployee;
	private String creditRating;
	
	private Address address;
	private Set<Contact> contacts;
	
	private Industry industry;

	private Staff staff;
	
	public Customer() {
	}

	public Customer(int customerId, String customerName, int yearRegistered, int quantityOrdered, int noOfEmployee,
			String creditRating, Address address, Industry industry, Staff staff) {
		super();
		this.customerId = customerId;
		this.customerName = customerName;
		this.yearRegistered = yearRegistered;
		this.quantityOrdered = quantityOrdered;
		this.noOfEmployee = noOfEmployee;
		this.creditRating = creditRating;
		this.address = address;
		this.contacts = new HashSet<>();
		this.industry = industry;
		this.staff = staff;
	}

//	@Id
//    @GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Id
	@Column(name = "customer_id")
	public int getCustomerId() {
		return customerId;
	}

	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}

	@Column(name = "customer_name")
	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	@Column(name = "year_registered")
	public int getYearRegistered() {
		return yearRegistered;
	}

	public void setYearRegistered(int yearRegistered) {
		this.yearRegistered = yearRegistered;
	}

	@Column(name = "quantity_ordered")
	public int getQuantityOrdered() {
		return quantityOrdered;
	}

	public void setQuantityOrdered(int quantityOrdered) {
		this.quantityOrdered = quantityOrdered;
	}

	@Column(name = "no_of_employee")
	public int getNoOfEmployee() {
		return noOfEmployee;
	}

	public void setNoOfEmployee(int noOfEmployee) {
		this.noOfEmployee = noOfEmployee;
	}

	@Column(name = "credit_rating")
	public String getCreditRating() {
		return creditRating;
	}

	public void setCreditRating(String creditRating) {
		this.creditRating = creditRating;
	}
	
	@Embedded
	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	@OneToMany(mappedBy = "customer", fetch = FetchType.EAGER, cascade = CascadeType.ALL )
	public Set<Contact> getContacts() {
		return contacts;
	}
	
	public void setContacts(Set<Contact> contacts) {
		this.contacts = contacts;
	}
	
	//Convert set to list for repeating in customerdetail.xhtml.
	public List<Contact> getContactsList(){
		return new ArrayList<Contact>(contacts);
	}

//	@ElementCollection
//    @CollectionTable(name = "INDUSTRY")
//    @Column(name = "VALUE")
	@ManyToOne
	public Industry getIndustry() {
		return industry;
	}

	public void setIndustry(Industry industry) {
		this.industry = industry;
	}

	@ManyToOne
	public Staff getStaff() {
		return staff;
	}

	public void setStaff(Staff staff) {
		this.staff = staff;
	}

	@Override
	public String toString() {
		return "[Customer Id] " + customerId + "   [Customer Name] " + customerName + "   [Year Registered] "
				+ yearRegistered + "   [Quantity Ordered] " + quantityOrdered + "   [No of Employee] " + noOfEmployee
				+ "   [Credit Rating] " + creditRating;
	}

}
