package fit5042.assign.repository.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotEmpty;

@Entity
@NamedQueries({
    @NamedQuery(name = Industry.GET_ALL_QUERY_NAME, query = "SELECT p FROM Industry p order by p.industryId")})
public class Industry implements Serializable{

	public static final String GET_ALL_QUERY_NAME = "Industry.getAll";
	
	private int industryId;
	private String industryType;
	
	private Set<Customer> customers;

	public Industry() {
		super();
	}

	public Industry(int industryId, String industryType, Set<Customer> customers) {
		super();
		this.industryId = industryId;
		this.industryType = industryType;
		this.customers = new HashSet<>();
	}

	
//    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Id
	@Column(name = "industry_id")
	public int getIndustryId() {
		return industryId;
	}

	public void setIndustryId(int industryId) {
		this.industryId = industryId;
	}

	@Column(name = "industry_type")
	@NotEmpty(message = "Please enter an Industry Type.")
	public String getIndustryType() {
		return industryType;
	}

	public void setIndustryType(String industryType) {
		this.industryType = industryType;
	}

	@OneToMany(mappedBy = "industry", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	public Set<Customer> getCustomers() {
		return customers;
	}

	public void setCustomers(Set<Customer> customers) {
		this.customers = customers;
	}
	
	@Override
	public String toString() {
		return industryType;
	}
	
}
