package fit5042.assign.repository.entities;

import java.io.Serializable;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Embeddable;


@Embeddable
@Access(AccessType.PROPERTY)
public class Address implements Serializable{

	private String streetNo;
    private String streetName;
    private String district;
    private String zipcode;
    private String state;
	
    public Address() {
    	
	}

	public Address(String streetNo, String streetName, String district, String zipcode, String state) {
		super();
		this.streetNo = streetNo;
		this.streetName = streetName;
		this.district = district;
		this.zipcode = zipcode;
		this.state = state;
	}

	@Column(name = "street_no")
	public String getStreetNo() {
		return streetNo;
	}

	public void setStreetNo(String streetNo) {
		this.streetNo = streetNo;
	}

	@Column(name = "street_name")
	public String getStreetName() {
		return streetName;
	}

	public void setStreetName(String streetName) {
		this.streetName = streetName;
	}

	@Column(name = "district")
	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	@Column(name = "zipcode")
	public String getZipcode() {
		return zipcode;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}

	@Column(name = "state")
	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	@Override
	public String toString() {
		return streetNo + " " + streetName + ", " + district + ", "
				+ state + ", " + zipcode;
	}
    
    
	
}
