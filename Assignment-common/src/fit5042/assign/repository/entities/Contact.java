package fit5042.assign.repository.entities;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;


@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorColumn(name = "contact_type", discriminatorType = DiscriminatorType.STRING, length = 1)
@NamedQueries({
    @NamedQuery(name = Contact.GET_ALL_QUERY_NAME, query = "SELECT p FROM Contact p order by p.contactId")})
public class Contact implements Serializable {

	public static final String GET_ALL_QUERY_NAME = "Contact.getAll";
	
	private int contactId;
	private String firstName;
	private String lastName;
	private String position;
	private String email;

	private String phoneNo;
	private String preference;
	
	//ADD: for inheritance
	private String contactType; 
	
	private Customer customer;

	public Contact() {
		super();
	}

	public Contact(int contactId, String firstName, String lastName, String position, String email, String phoneNo,
			String preference, String contactType, Customer customer) {
		super();
		this.contactId = contactId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.position = position;
		this.email = email;
		this.phoneNo = phoneNo;
		this.preference = preference;
		this.contactType = contactType;
		this.customer = customer;
	}

	
//    @GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Id
	@Column(name = "contact_id")
	public int getContactId() {
		return contactId;
	}

	public void setContactId(int contactId) {
		this.contactId = contactId;
	}

	@Column(name = "first_name")
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	@Column(name = "last_name")
	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getName() {
		return firstName + " " + lastName;
	}
	
	public void setName(String firstName, String lastName) {
		this.firstName = firstName;
		this.lastName = lastName;
	}
	
	@Column(name = "position")
	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	@Column(name = "email")
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Column(name = "phone_no")
	public String getPhoneNo() {
		return phoneNo;
	}
	
	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}

	@Column(name = "preference")
	public String getPreference() {
		return preference;
	}

	public void setPreference(String preference) {
		this.preference = preference;
	}
	
	//ADD: for inheritance
	@Column(name = "contact_type")
	public String getContactType() {
		return contactType;
	}

	public void setContactType(String contactType) {
		this.contactType = contactType;
	}

	@ManyToOne
	public Customer getCustomer() {
		return customer;
	}
	
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	@Override
	public int hashCode() {
		final int prime = 53;
		int result = 7;
		result = prime * result + contactId;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
//		if (this == obj)
//			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Contact other = (Contact) obj;
		if (contactId != other.contactId)
			return false;
		return true;
	}

//	@Override
//	public String toString() {
//		return "[Contact Id] " + contactId + "\r\n" + "   [First Name] " + firstName + "\r\n" + "   [Last Name] " + lastName + "\r\n" + "   [Position] "
//				+ position + "\r\n" + "   [Email] " + email + "\r\n" + "   [Phone No] " + phoneNo + "\r\n" + "   [Preference] " + preference ;
//	}

	
}
