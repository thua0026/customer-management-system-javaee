package fit5042.assign.repository.entities;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrimaryKeyJoinColumn;

@Entity
@DiscriminatorValue(value = "D")
@PrimaryKeyJoinColumn(name = "contact_id")
@NamedQueries({
	@NamedQuery(name = ConDomestic.GET_ALL_CONDOMESTIC, query = "SELECT s FROM ConDomestic s") })
public class ConDomestic extends Contact{
	
	public static final String GET_ALL_CONDOMESTIC = "ConDomestic.getAll";
	
	private String state;

	public ConDomestic() {
		super();
	}
	
	public ConDomestic(int contactId, String firstName, String lastName, String position, String email, String phoneNo,
			String preference, String contactType, Customer customer, String state) {
		super(contactId, firstName, lastName, position, email, phoneNo,
				preference, contactType, customer);
		this.state = state;
	}

	@Column(name = "state")
	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}	

}
