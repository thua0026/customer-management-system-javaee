package fit5042.assign.repository.entities;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrimaryKeyJoinColumn;

@Entity
@DiscriminatorValue(value = "I")
@PrimaryKeyJoinColumn(name = "contact_id")
@NamedQueries({
	@NamedQuery(name = ConInternational.GET_ALL_CONINTERNATIONAL, query = "SELECT s FROM ConInternational s") })
public class ConInternational extends Contact{

public static final String GET_ALL_CONINTERNATIONAL = "ConInternational.getAll";
	
	private String country;

	public ConInternational() {
		super();
	}
	
	public ConInternational(int contactId, String firstName, String lastName, String position, String email, String phoneNo,
			String preference, String contactType, Customer customer, String country) {
		super(contactId, firstName, lastName, position, email, phoneNo,
				preference, contactType, customer);
		this.country = country;
	}

	@Column(name = "country")
	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}
	
}
