package fit5042.assign.repository.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;

@Entity
@NamedQueries({
    @NamedQuery(name = Staff.GET_ALL_QUERY_NAME, query = "SELECT p FROM Staff p order by p.id")})
public class Staff implements Serializable {

	public static final String GET_ALL_QUERY_NAME = "Staff.getAll";
	
	private int id;
	private String username;
	private String password;
	private String userType;
	
	private Set<Customer> customers;
	
	public Staff() {
		super();
	}

	public Staff(int id, String username, String password, String userType) {
		super();
		this.id = id;
		this.username = username;
		this.password = password;
		this.userType = userType;
		this.customers = new HashSet<>();
	}

	
//    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Id
	@Column(name = "staff_id")
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Column(name = "username")
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	@Column(name = "password")
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	@Column(name = "user_type")
	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}
	
	@OneToMany(mappedBy = "staff", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	public Set<Customer> getCustomers() {
		return customers;
	}

	public void setCustomers(Set<Customer> customers) {
		this.customers = customers;
	}

	//Convert set to list for repeating in staffdetail.xhtml.
	public List<Customer> getCustomersList(){
		return new ArrayList<Customer>(customers);
	}
	
	@Override
	public String toString() {
		return username;
	}


	
	
}
