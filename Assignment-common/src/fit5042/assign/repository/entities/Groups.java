package fit5042.assign.repository.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

@Entity
@NamedQueries({
    @NamedQuery(name = Groups.GET_ALL_QUERY_NAME, query = "SELECT p FROM Groups p order by p.id")})
public class Groups implements Serializable{

	public static final String GET_ALL_QUERY_NAME = "Groups.getAll";
	
	private int id;
	private String username;
	private String groupname;
	
	public Groups() {
		super();
	}

	public Groups(int id, String username, String groupname) {
		super();
		this.id = id;
		this.username = username;
		this.groupname = groupname;
	}

	
//    @GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Id
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getGroupname() {
		return groupname;
	}

	public void setGroupname(String groupname) {
		this.groupname = groupname;
	}
	
	
	
}
