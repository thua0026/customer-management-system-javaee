package fit5042.assign.repository;

import java.util.List;
import java.util.Set;

import javax.ejb.Remote;

import fit5042.assign.repository.entities.ConDomestic;
import fit5042.assign.repository.entities.ConInternational;
import fit5042.assign.repository.entities.Contact;
import fit5042.assign.repository.entities.Customer;

@Remote
public interface ContactRepository {

	public void addContact(Contact contact) throws Exception;
	
	public Contact searchContactById(int contactId) throws Exception;
	
	public List<Contact> getAllContacts() throws Exception;
	
	public void removeContact(int contactId) throws Exception;
	
	public void editContact(Contact contact) throws Exception;
	
//	----------------------------------------------

	public List<Customer> getAllCustomers() throws Exception;

	public Set<Contact> searchContactByCustomer(Customer customer) throws Exception;
	
//	==================================================
	
	public void addConDomestic(ConDomestic conDomestic) throws Exception;
	
	public ConDomestic searchConDomesticById(int contactId) throws Exception;
	
	public List<ConDomestic> getAllConDomestics() throws Exception;
	
	public void removeConDomestic(int contactId) throws Exception;
	
	public void editConDomestic(ConDomestic conDomestic) throws Exception;
	
//	==================================================
	
	public void addConInternational(ConInternational conInternational) throws Exception;
	
	public ConInternational searchConInternationalById(int contactId) throws Exception;
	
	public List<ConInternational> getAllConInternationals() throws Exception;
	
	public void removeConInternational(int contactId) throws Exception;
	
	public void editConInternational(ConInternational conInternational) throws Exception;
	
}
