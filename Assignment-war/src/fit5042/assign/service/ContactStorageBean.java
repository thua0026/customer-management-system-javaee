package fit5042.assign.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import javax.ejb.LocalBean;
import javax.ejb.Singleton;

/**
 * Session Bean implementation class ContactStorageBean
 */
@Singleton
@LocalBean
public class ContactStorageBean {

//	private int contactCount = 0;
	
	private Set<String> nameList = new TreeSet<String>();
	
    /**
     * Default constructor. 
     */
    public ContactStorageBean() {
    	//https://howtodoinjava.com/java/collections/arraylist/add-multiple-elements-arraylist/
    	List<String> tempList = Arrays.asList("Canada", "United States", "Singapore", "Malaysia","Japan",
    			"United Kingdom","Germany","Sweden","France","New Zealand");
    	nameList.addAll(tempList);
    }

    public Set<String> getNameList() {
		return nameList;
	}

	public void setNameList(Set<String> nameList) {
		this.nameList = nameList;
	}

	public void addNameList(String name) {
		nameList.add(name);
	}
	
	public void removeNameListOne(String name) {
		nameList.remove(name);
	}
    
//	public int getContactCount() {
//		return contactCount;
//	}
//
//	public void setContactCount(int contactCount) {
//		this.contactCount = contactCount;
//	}
    
    

}
