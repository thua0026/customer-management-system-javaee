package fit5042.assign.service;

import java.util.ArrayList;
import java.util.Set;

import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;

@Path("greeting")
public class ContactREST {
    @SuppressWarnings("unused")
    @Context
    private UriInfo context;
    
    @EJB
    private ContactStorageBean contactStorageBean;

    /**
     * Default constructor. 
     */
    public ContactREST() {

    }

    /**
     * Retrieves representation of an instance of ContactREST
     * @return an instance of String
     */
    @GET
    @Produces("text/html")
    public String getHtml() {
//        throw new UnsupportedOperationException();
//    	String outputString =  "<html><body><h1>There are " + contactStorageBean.getContactCount() + " contact(s) in the database !</h1></body></html>";
//    	return outputString;
    	Set<String> nameList = contactStorageBean.getNameList();
    	String outputString = "";
    	for (String name : nameList) {
    		outputString = outputString + name + "; ";
    	}
    	return outputString;
    	
    }

    /**
     * PUT method for updating or creating an instance of ContactREST
     * @param content representation for the resource
     * @return an HTTP response with content of the updated or created resource.
     */
    @PUT
    @Consumes("text/html")
    public void putHtml(String content) {
    }
    
    //add a country
    @POST
    @Consumes("application/x-www-form-urlencoded")
    public void setPostCount(@FormParam("country") String country) {
//    	contactStorageBean.setContactCount(Integer.parseInt(contactCount));
    	contactStorageBean.addNameList(country);
    }

    
    //delete a specific country
    @DELETE
    public void removeCountry (@QueryParam("country") String country) {
    	contactStorageBean.removeNameListOne(country);
    }
}

