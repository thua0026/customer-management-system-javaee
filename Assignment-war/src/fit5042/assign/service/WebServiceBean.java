package fit5042.assign.service;

import java.util.ArrayList;

import javax.faces.bean.SessionScoped;
import javax.inject.Named;

import fit5042.assign.service.client.WebServiceClient;

@Named(value = "webServiceBean")
@SessionScoped
public class WebServiceBean {

//	private int contactCount;
	private WebServiceClient webServiceClient;
	
	private String country;
	
	public WebServiceBean() {
		webServiceClient = new WebServiceClient();
	}

	public String getCountry() {
		return country;
	}
	
	public void setCountry(String country) {
		this.country = country;
	}
	
	
//	public int getContactCount() {
//		return contactCount;
//	}

//	public void setContactCount(int contactCount) {
//		this.contactCount = contactCount;
//	}

//	public void setContactCount() {
//		
//	}
	
//	public void setWebServiceClient() {
//		webServiceClient = new WebServiceClient();
//		webServiceClient.setPostCount(Integer.toString(getContactCount()));
//	}

//	public void setWebServiceClient(int contactCount) {
//		webServiceClient = new WebServiceClient();
//		webServiceClient.setPostCount(Integer.toString(contactCount));
//	}
	
	public WebServiceClient getWebServiceClient() {
		return webServiceClient;
	}
	
	public void setWebServiceClient(WebServiceClient webServiceClient) {
		this.webServiceClient = webServiceClient;
	}
	
	//add a country
	public void addCountry() {
//		webServiceClient = new WebServiceClient();
		webServiceClient.setPostCountry(getCountry());
	}
	
	//get country list (String)
	public String getCountryFromClient() {
		String countryList =  webServiceClient.getHtml();
		return countryList;
	}
	
	//get country list (ArrayList)
	public ArrayList<String> getCountryArray() {
		String countryList =  webServiceClient.getHtml();
		ArrayList<String> countries = new ArrayList<String>();
		String[] countryDetail = countryList.split("; ");
		for (String country : countryDetail) {
			countries.add(country);
		}
		return countries;
	}
	
	//remove a country
	public void removeCountryFromClient() {
		webServiceClient.removeCountry(getCountry());
	}
}
