package fit5042.assign.controller;

import java.util.ArrayList;
import java.util.Set;

import javax.el.ELContext;
import javax.enterprise.context.ApplicationScoped;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import fit5042.assign.mbeans.IndustryManagedBean;
import fit5042.assign.repository.entities.Industry;

@Named(value = "industryApplication")
@ApplicationScoped
public class IndustryApplication {

	@ManagedProperty(value = "#{industryManagedBean}")
	IndustryManagedBean industryManagedBean;
	
	private ArrayList<Industry> industries;
	
	private boolean showForm = true;

    public boolean isShowForm() {
        return showForm;
    }
    
    public IndustryApplication() throws Exception{
    	industries = new ArrayList<>();
    	
    	//instantiate industryManagedBean
        ELContext elContext = FacesContext.getCurrentInstance().getELContext();
        industryManagedBean = (IndustryManagedBean) FacesContext.getCurrentInstance().getApplication()
                .getELResolver().getValue(elContext, null, "industryManagedBean");
        
        //get industries from db
        updateIndustryList();      
    	
    }

	public ArrayList<Industry> getIndustries() {
		return industries;
	}

	public void setIndustries(ArrayList<Industry> industries) {
		this.industries = industries;
	}
    
	public void updateIndustryList() {
		industries.clear();
		for (Industry industry: industryManagedBean.getAllIndustries()) {
			industries.add(industry);
		}
		setIndustries(industries);
	}
	
	public void searchIndustryById (int industryId) {
		industries.clear();
		
		Industry industry = industryManagedBean.searchIndustryById(industryId);
		if (industry != null) {
			industries.add(industry);
		}
	}

//	public void searchIndustryByCustomerId(int industryId) {
//		industries.clear();
//		
//		Set<Industry> industries = industryManagedBean.searchIndustryByIndustryId(industryId);
//		if (industries != null) {
//			for (Industry industry: industries) {
//				this.industries.add(industry);
//			}			
//		}		
//	}

    
}
