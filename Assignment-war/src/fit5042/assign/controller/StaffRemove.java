package fit5042.assign.controller;

import javax.el.ELContext;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedProperty;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import fit5042.assign.mbeans.StaffManagedBean;
import fit5042.assign.repository.entities.Staff;

@RequestScoped
@Named("staffRemove")
public class StaffRemove {

	@ManagedProperty(value = "#{staffManagedBean}")
	StaffManagedBean staffManagedBean;
	
	private boolean showForm = true;
	
	private Staff staff;
	
	StaffApplication app;
	
	private UIComponent component;

	public StaffRemove() {
		//get application context bean
		ELContext context = FacesContext.getCurrentInstance().getELContext();
		app = (StaffApplication) FacesContext.getCurrentInstance()
                .getApplication()
                .getELResolver()
                .getValue(context, null, "staffApplication");
		
		app.updateStaffList();
		
		//instantiate staffManagedBean
        ELContext elContext = FacesContext.getCurrentInstance().getELContext();
        staffManagedBean = (StaffManagedBean) FacesContext.getCurrentInstance().getApplication()
                .getELResolver().getValue(elContext, null, "staffManagedBean");
	}
	
	public void staffRemove(int staffId) {
		try {
			//remove this contact from db via EJB
			staffManagedBean.removeStaff(staffId);
			
			//refresh the list in Application bean
			app.updateStaffList();
			
			//show confirmation message.
			FacesContext.getCurrentInstance().addMessage(component.getClientId(), new FacesMessage("Staff and its customers and contacts DELETED!!"));
		} catch (Exception e) {
			
		}
		showForm = true;
	}

	public boolean isShowForm() {
        return showForm;
    }
	
	public Staff getStaff() {
		return staff;
	}

	public void setStaff(Staff staff) {
		this.staff = staff;
	}

	public UIComponent getComponent() {
		return component;
	}

	public void setComponent(UIComponent component) {
		this.component = component;
	}
	
	
}
