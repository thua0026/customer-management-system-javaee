package fit5042.assign.controller;

import javax.el.ELContext;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedProperty;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import fit5042.assign.mbeans.ContactManagedBean;
import fit5042.assign.repository.entities.ConDomestic;
import fit5042.assign.repository.entities.ConInternational;
import fit5042.assign.repository.entities.Contact;

@RequestScoped
@Named("contactRemove")
public class ContactRemove {

	@ManagedProperty(value = "#{contactManagedBean}")
	ContactManagedBean contactManagedBean;
	
	private boolean showForm = true;
	
	private Contact contact;
	
	//FOR inheritance
	private ConDomestic conDomestic;
	private ConInternational conInternational;
	
	ContactApplication app;
	
	private UIComponent component;

	public ContactRemove() {
		//get application context bean
		ELContext context = FacesContext.getCurrentInstance().getELContext();
		app
        = (ContactApplication) FacesContext.getCurrentInstance()
                .getApplication()
                .getELResolver()
                .getValue(context, null, "contactApplication");
		
		app.updateContactList();
		app.updateConDomesticList();
        app.updateConInternationalList();
		
		//instantiate contactManagedBean
        ELContext elContext = FacesContext.getCurrentInstance().getELContext();
        contactManagedBean = (ContactManagedBean) FacesContext.getCurrentInstance().getApplication()
                .getELResolver().getValue(elContext, null, "contactManagedBean");
		
	}
	
	public UIComponent getComponent() {
        return component;
    }
    
    public void setComponent(UIComponent component) {
        this.component = component;
    }
	
	public void contactRemove(int contactId) {
		try {
			//remove this contact from db via EJB
			contactManagedBean.removeContact(contactId);
			
			//refresh the list in Application bean
			app.updateContactList();
			
			//show confirmation message.
			FacesContext.getCurrentInstance().addMessage(component.getClientId(), new FacesMessage("Contact DELETED!!"));
			
		} catch (Exception e) {
			
		}
		
		showForm = true;
	}
	
	
//	---------------------------------------------------------
	public void conDomesticRemove(int contactId) {
		try {
			//remove this contact from db via EJB
			contactManagedBean.removeConDomestic(contactId);
			
			//refresh the list in CustomerApplication bean
			app.updateContactList();
			app.updateConDomesticList();
	        app.updateConInternationalList();
			
			//show confirmation message.
			FacesContext.getCurrentInstance().addMessage(component.getClientId(), new FacesMessage("Domestic Contact DELETED!!"));
			
		} catch (Exception e) {
			
		}
		
		showForm = true;
	}
	
	
	
	public void conInternationalRemove(int contactId) {
		try {
			//remove this contact from db via EJB
			contactManagedBean.removeConInternational(contactId);
			
			//refresh the list in CustomerApplication bean
			app.updateContactList();
			app.updateConDomesticList();
	        app.updateConInternationalList();
	        
			//show confirmation message.
			FacesContext.getCurrentInstance().addMessage(component.getClientId(), new FacesMessage("International Contact DELETED!!"));
			
		} catch (Exception e) {
			
		}
		
		showForm = true;
	}
	
//	---------------------------------------------------------
	
	public boolean isShowForm() {
        return showForm;
    }

	public Contact getContact() {
		return contact;
	}

	public void setContact(Contact contact) {
		this.contact = contact;
	}

	public ConDomestic getConDomestic() {
		return conDomestic;
	}

	public void setConDomestic(ConDomestic conDomestic) {
		this.conDomestic = conDomestic;
	}

	public ConInternational getConInternational() {
		return conInternational;
	}

	public void setConInternational(ConInternational conInternational) {
		this.conInternational = conInternational;
	}
	
	
	
}
