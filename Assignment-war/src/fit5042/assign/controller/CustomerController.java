package fit5042.assign.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.el.ELContext;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import fit5042.assign.mbeans.ContactManagedBean;
import fit5042.assign.repository.entities.ConDomestic;
import fit5042.assign.repository.entities.ConInternational;
import fit5042.assign.repository.entities.Contact;
import fit5042.assign.repository.entities.Customer;

@Named(value = "customerController")
@RequestScoped
public class CustomerController {

	private int customerID; // the customer Index 

	public int getCustomerID() {
		return customerID;
	}

	public void setCustomerID(int customerID) {
		this.customerID = customerID;
	}
	
	private Customer customer;
	
	public CustomerController() {
		
		//Assign customerID via GET parameter
		customerID = Integer.valueOf(FacesContext.getCurrentInstance()
                .getExternalContext()
                .getRequestParameterMap()
                .get("customerID"));
		//assign customer based on the customerID.
		customer = getCustomer();
	}
	
	public Customer getCustomer() {
		if (customer == null) {
			//get application context bean
			ELContext context = FacesContext.getCurrentInstance().getELContext();
			CustomerApplication app
            = (CustomerApplication) FacesContext.getCurrentInstance()
                    .getApplication()
                    .getELResolver()
                    .getValue(context, null, "customerApplication");
			//minus 1 in customerID since it added i in (customerlist.xhtml)
			return app.getCustomers().get(--customerID);
		}
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
	
	public List<Contact> getContacts() {
		int customerId = customer.getCustomerId();
		List<Contact> outputs = new ArrayList<Contact>();
		
		ELContext elContext = FacesContext.getCurrentInstance().getELContext();
        ContactManagedBean contactManagedBean = (ContactManagedBean) FacesContext.getCurrentInstance().getApplication()
                .getELResolver().getValue(elContext, null, "contactManagedBean");
       
        Set<Contact> contacts = contactManagedBean.searchContactByCustomerId(customerId);
		if (contacts != null) {
			for (Contact contact: contacts) {
				outputs.add(contact);
			}			
		}		
        
        return outputs;
	}
	
}
