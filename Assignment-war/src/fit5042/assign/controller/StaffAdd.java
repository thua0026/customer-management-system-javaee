package fit5042.assign.controller;

import javax.el.ELContext;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedProperty;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import com.sun.webkit.ContextMenu.ShowContext;

import fit5042.assign.mbeans.StaffManagedBean;

@RequestScoped
@Named("staffAdd")
public class StaffAdd {

	@ManagedProperty(value = "#{staffManagedBean}")
	StaffManagedBean staffManagedBean;
	
	private boolean showForm = true;
	
	private StaffBean staff;
	
	StaffApplication app;
	
	private UIComponent component;
	
	public StaffAdd() {
		//get application context bean
		ELContext context = FacesContext.getCurrentInstance().getELContext();
		app = (StaffApplication) FacesContext.getCurrentInstance()
                .getApplication()
                .getELResolver()
                .getValue(context, null, "staffApplication");
		
		//instantiate staffManagedBean
        ELContext elContext = FacesContext.getCurrentInstance().getELContext();
        staffManagedBean = (StaffManagedBean) FacesContext.getCurrentInstance().getApplication()
                .getELResolver().getValue(elContext, null, "staffManagedBean");
	}

	public void staffAdd(StaffBean localStaff) {
		try {
			int check = staffManagedBean.checkUsername(localStaff.getUsername(), -1);
			if(check==1){
				FacesContext.getCurrentInstance().addMessage(component.getClientId(), new FacesMessage("Username cannot be null !!"));
			}else if (check==2) {
				FacesContext.getCurrentInstance().addMessage(component.getClientId(), new FacesMessage("Username exist!! please enter another username"));
			}else {
				staffManagedBean.addStaff(localStaff);
				
//				app.updateStaffList();
				
				FacesContext.getCurrentInstance().addMessage(component.getClientId(), new FacesMessage("Staff Added! Please click CLOSE button!!"));
			}	
			
		} catch (Exception e) {
			
		}
		showForm = true;
	}
	
	public void staffAddFirst() {
		try {
			staffManagedBean.addStaffFirst();
			
			app.updateStaffList();
			
			FacesContext.getCurrentInstance().addMessage(component.getClientId(), new FacesMessage("Staff Added!"));
		} catch (Exception e) {
			
		}
		showForm = true;
	}	
	

	public StaffBean getStaff() {
		return staff;
	}

	public void setStaff(StaffBean staff) {
		this.staff = staff;
	}

	public UIComponent getComponent() {
		return component;
	}

	public void setComponent(UIComponent component) {
		this.component = component;
	}
	
	public boolean isShowForm() {
        return showForm;
    }
	
	
}
