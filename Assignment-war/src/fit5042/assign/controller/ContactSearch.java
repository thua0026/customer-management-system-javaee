package fit5042.assign.controller;

import javax.el.ELContext;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import fit5042.assign.repository.entities.Contact;

@Named(value = "contactSearch")
@RequestScoped
public class ContactSearch {

	private boolean showForm = true;
	
	private Contact contact;
	
	ContactApplication app;

	public ContactSearch() {
		//get application context bean
		ELContext context = FacesContext.getCurrentInstance().getELContext();
		app = (ContactApplication) FacesContext.getCurrentInstance()
                .getApplication()
                .getELResolver()
                .getValue(context, null, "contactApplication");
		
		app.updateContactList();
		app.updateConDomesticList();
        app.updateConInternationalList();
	}

	public ContactApplication getApp() {
		return app;
	}

	public void setApp(ContactApplication app) {
		this.app = app;
	}
	
	public void searchAll() {
		try {
            //return all properties from db via EJB
            app.updateContactList();
            app.updateConDomesticList();
	        app.updateConInternationalList();
        } catch (Exception ex) {

        }
        showForm = true;
	}
	
//	-------------------------------------------------
	
	private int contactId;

	public int getContactId() {
		return contactId;
	}

	public void setContactId(int contactId) {
		this.contactId = contactId;
	}
	
	public void searchContactById (int contactId) {
		try {
			app.searchContactById(contactId);
		} catch (Exception e) {
			
		}
		showForm = true;
	}
	
//	-------------------------------------------------
	
	public void searchContactByCustomerId(int customerId) {
		try {
        	int p = customerId;
            //search all customer by contact from db via EJB 
            app.searchContactByCustomerId(customerId);
        } catch (Exception ex) {

        }
        showForm = true;
	}
	
//	------------------------------------------------------
	
	private String preference;
	
	private String position;

	public String getPreference() {
		return preference;
	}

	public void setPreference(String preference) {
		this.preference = preference;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}
	
	public void searchContactsByPositionAndPreference(String position, String preference) {
		try {
        	app.searchContactsByPositionAndPreference(position, preference);
        } catch (Exception ex) {

        }
        showForm = true;
	}
	
	
}
