package fit5042.assign.controller;

import javax.el.ELContext;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedProperty;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import fit5042.assign.mbeans.ContactManagedBean;
import fit5042.assign.mbeans.StaffManagedBean;
import fit5042.assign.repository.entities.Contact;

@RequestScoped
@Named("contactAdd")
public class ContactAdd {
	
	@ManagedProperty(value = "#{contactManagedBean}")
	ContactManagedBean contactManagedBean;
	
	private boolean showForm = true;
	
//	private Contact contact;
	
	private ContactBean contact;
	
	ContactApplication app;
	
	private UIComponent component;

	public ContactAdd() {
		//get application context bean
		ELContext context = FacesContext.getCurrentInstance().getELContext();
		app = (ContactApplication) FacesContext.getCurrentInstance()
                .getApplication()
                .getELResolver()
                .getValue(context, null, "contactApplication");
		
		//instantiate contactManagedBean
        ELContext elContext = FacesContext.getCurrentInstance().getELContext();
        contactManagedBean = (ContactManagedBean) FacesContext.getCurrentInstance().getApplication()
                .getELResolver().getValue(elContext, null, "contactManagedBean");
        
	}
	
	
	
	public UIComponent getComponent() {
		return component;
	}

	public void setComponent(UIComponent component) {
		this.component = component;
	}


	public void contactAdd(ContactBean localContact) {
		try {
//			localContact.setContactId(contactManagedBean.getFinalContactId()+1);
			
			contactManagedBean.addContact(localContact);
			
			app.updateContactList();
			
			FacesContext.getCurrentInstance().addMessage(component.getClientId(), new FacesMessage("Contact Added!"));
			
		} catch(Exception e) {
			
		}
		
		showForm = true;
	}
	
	public void contactAddFirst() {
		try {
//			localContact.setContactId(contactManagedBean.getFinalContactId()+1);

			contactManagedBean.addContactFirst();
			
			app.updateContactList();
			
			FacesContext.getCurrentInstance().addMessage(component.getClientId(), new FacesMessage("Contact Added!"));
			
		} catch(Exception e) {
			
		}
		
		showForm = true;
	}
	
//	-----------------------------------------------
	//FOR INHERITANCE
	
	public void conDomesticAdd(ContactBean localContact) {
		try {
			
			contactManagedBean.addConDomestic(localContact);
			
			app.updateContactList();
			//FOR inheritance mapping.
	        app.updateConDomesticList();
	        app.updateConInternationalList();
			
			FacesContext.getCurrentInstance().addMessage(component.getClientId(), new FacesMessage("Domestic Contact Added!"));
			
		} catch(Exception e) {
			
		}
		
		showForm = true;
	}
	
	
	public void conInternationalAdd(ContactBean localContact) {
		try {
			
			contactManagedBean.addConInternational(localContact);
			
			app.updateContactList();
			//FOR inheritance mapping.
	        app.updateConDomesticList();
	        app.updateConInternationalList();
			
			FacesContext.getCurrentInstance().addMessage(component.getClientId(), new FacesMessage("International Contact Added!"));
			
		} catch(Exception e) {
			
		}
		
		showForm = true;
	}	
	
//	-----------------------------------------------
	
	public boolean isShowForm() {
        return showForm;
    }

//	public Contact getContact() {
//		return contact;
//	}
//
//	public void setContact(Contact contact) {
//		this.contact = contact;
//	}
	
	public ContactBean getContact() {
		return contact;
	}

	public void setContact(ContactBean contact) {
		this.contact = contact;
	}
	


}
