package fit5042.assign.controller;

import java.io.Serializable;
import java.util.Set;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

import fit5042.assign.repository.entities.Address;
import fit5042.assign.repository.entities.Contact;
import fit5042.assign.repository.entities.Customer;
import fit5042.assign.repository.entities.Industry;
import fit5042.assign.repository.entities.Staff;

@RequestScoped
@Named(value = "customerBean")
public class CustomerBean implements Serializable{

	private int customerId;
	private String customerName;
	private int yearRegistered;
	private int quantityOrdered;
	private int noOfEmployee;
	private String creditRating;
	
	private Address address;
	private String streetNo;
    private String streetName;
    private String district;
    private String zipcode;
    private String state;
	
	private Set<Contact> contacts;
	private int contactId;
	
	private Industry industry;
	private int industryId;

	private Staff staff;
	private int id;
	
	private Set<Customer> customers;

	public CustomerBean() {
		super();
	}

	public CustomerBean(int customerId, String customerName, int yearRegistered, int quantityOrdered, int noOfEmployee,
			String creditRating, Address address, Set<Contact> contacts, Industry industry, Staff staff) {
		super();
		this.customerId = customerId;
		this.customerName = customerName;
		this.yearRegistered = yearRegistered;
		this.quantityOrdered = quantityOrdered;
		this.noOfEmployee = noOfEmployee;
		this.creditRating = creditRating;
		this.address = address;
		this.contacts = contacts;
		this.industry = industry;
		this.staff = staff;
	}

	public int getCustomerId() {
		return customerId;
	}

	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public int getYearRegistered() {
		return yearRegistered;
	}

	public void setYearRegistered(int yearRegistered) {
		this.yearRegistered = yearRegistered;
	}

	public int getQuantityOrdered() {
		return quantityOrdered;
	}

	public void setQuantityOrdered(int quantityOrdered) {
		this.quantityOrdered = quantityOrdered;
	}

	public int getNoOfEmployee() {
		return noOfEmployee;
	}

	public void setNoOfEmployee(int noOfEmployee) {
		this.noOfEmployee = noOfEmployee;
	}

	public String getCreditRating() {
		return creditRating;
	}

	public void setCreditRating(String creditRating) {
		this.creditRating = creditRating;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public String getStreetNo() {
		return streetNo;
	}

	public void setStreetNo(String streetNo) {
		this.streetNo = streetNo;
	}

	public String getStreetName() {
		return streetName;
	}

	public void setStreetName(String streetName) {
		this.streetName = streetName;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public String getZipcode() {
		return zipcode;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public Set<Contact> getContacts() {
		return contacts;
	}

	public void setContacts(Set<Contact> contacts) {
		this.contacts = contacts;
	}

	public Industry getIndustry() {
		return industry;
	}

	public void setIndustry(Industry industry) {
		this.industry = industry;
	}

	public Staff getStaff() {
		return staff;
	}

	public void setStaff(Staff staff) {
		this.staff = staff;
	}

	public Set<Customer> getCustomers() {
		return customers;
	}

	public void setCustomers(Set<Customer> customers) {
		this.customers = customers;
	}

	public int getContactId() {
		return contactId;
	}

	public void setContactId(int contactId) {
		this.contactId = contactId;
	}

	public int getIndustryId() {
		return industryId;
	}

	public void setIndustryId(int industryId) {
		this.industryId = industryId;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	
	
}
