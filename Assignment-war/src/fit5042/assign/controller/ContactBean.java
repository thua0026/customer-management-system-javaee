package fit5042.assign.controller;

import java.io.Serializable;
import java.util.Set;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import fit5042.assign.repository.entities.Address;
import fit5042.assign.repository.entities.Contact;
import fit5042.assign.repository.entities.Customer;

@RequestScoped
@Named(value = "contactBean")
public class ContactBean implements Serializable{

	private int contactId;
	private String firstName;
	private String lastName;
	private String position;
	private String email;
	private String phoneNo;
	private String preference;
	
	private Customer customer;
	private int customerId;
//	private String customerName;
//	private int yearRegistered;
//	private int quantityOrdered;
//	private int noOfEmployee;
//	private String creditRating;
	
//	private Address address;
	
	private String country;
	private String state;
	
	private Set<Contact> contacts;

	public ContactBean() {
		super();
	}

	public ContactBean(int contactId, String firstName, String lastName, String position, String email, String phoneNo,
			String preference, Customer customer, int customerId, String country, String state, Set<Contact> contacts) {
		super();
		this.contactId = contactId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.position = position;
		this.email = email;
		this.phoneNo = phoneNo;
		this.preference = preference;
		this.customer = customer;
		this.customerId = customerId;
		this.country = country;
		this.state = state;
		this.contacts = contacts;
	}

	public int getContactId() {
		return contactId;
	}

	public void setContactId(int contactId) {
		this.contactId = contactId;
	}

	@NotEmpty
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	@NotEmpty
	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	@NotEmpty
	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	@NotEmpty
	@Email
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	//https://stackoverflow.com/questions/25518224/how-check-string-is-integer-or-not-by-using-spring-annotations/33622081
	@Pattern(regexp="^(0|[1-9][0-9]*)$", message = "must not empty; must only contains numbers.")
	public String getPhoneNo() {
		return phoneNo;
	}

	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}

	@NotEmpty
	public String getPreference() {
		return preference;
	}

	public void setPreference(String preference) {
		this.preference = preference;
	}

	@NotEmpty
	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public Set<Contact> getContacts() {
		return contacts;
	}

	public void setContacts(Set<Contact> contacts) {
		this.contacts = contacts;
	}

	@NotNull
	public int getCustomerId() {
		return customerId;
	}

	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}


//	-----------------------------------------
	// FOR INHERITANCE
	
	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	
	
}
