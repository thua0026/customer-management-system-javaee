package fit5042.assign.controller;

import javax.el.ELContext;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedProperty;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import fit5042.assign.mbeans.CustomerManagedBean;
import fit5042.assign.repository.entities.Customer;

@RequestScoped
@Named("customerRemove")
public class CustomerRemove {
	
	@ManagedProperty(value = "#{customerManagedBean}")
	CustomerManagedBean customerManagedBean;
	
	private boolean showForm = true;
	
	private Customer customer;
	
	CustomerApplication app;
	
	private UIComponent component;
	
	public UIComponent getComponent() {
		return component;
	}

	public void setComponent(UIComponent component) {
		this.component = component;
	}

	public CustomerRemove() {
		//get application context bean
		ELContext context = FacesContext.getCurrentInstance().getELContext();
		app
        = (CustomerApplication) FacesContext.getCurrentInstance()
                .getApplication()
                .getELResolver()
                .getValue(context, null, "customerApplication");
		
		app.updateCustomerList();
		
		//instantiate customerManagedBean
        ELContext elContext = FacesContext.getCurrentInstance().getELContext();
        customerManagedBean = (CustomerManagedBean) FacesContext.getCurrentInstance().getApplication()
                .getELResolver().getValue(elContext, null, "customerManagedBean");
		
	}
	
	public void customerRemove(int customerId) {
		try {
			//remove this customer from db via EJB
			customerManagedBean.removeCustomer(customerId);
			
			//refresh the list in CustomerApplication bean
			app.updateCustomerList();
			
			//show confirmation message.
			FacesContext.getCurrentInstance().addMessage(component.getClientId(), new FacesMessage("Customer and its' Contact DELETED!!"));
			
		} catch (Exception e) {
			
		}
		
		showForm = true;
	}

	public boolean isShowForm() {
        return showForm;
    }
	
	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
	
	
	
}
