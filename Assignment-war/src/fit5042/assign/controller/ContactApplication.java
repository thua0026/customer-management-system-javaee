package fit5042.assign.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.el.ELContext;
import javax.enterprise.context.ApplicationScoped;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import fit5042.assign.mbeans.ContactManagedBean;
import fit5042.assign.mbeans.StaffManagedBean;
import fit5042.assign.repository.entities.ConDomestic;
import fit5042.assign.repository.entities.ConInternational;
import fit5042.assign.repository.entities.Contact;
import fit5042.assign.repository.entities.Customer;

@Named(value = "contactApplication")
@ApplicationScoped
public class ContactApplication {

	@ManagedProperty(value = "#{contactManagedBean}")
	ContactManagedBean contactManagedBean;
	
	@ManagedProperty(value = "#{staffManagedBean}")
	StaffManagedBean staffManagedBean;
	
	private ArrayList<Contact> contacts;
	
	// FOR inheritance mapping.
	private ArrayList<ConDomestic> conDomestics;
	private ArrayList<ConInternational> conInternationals;
	
	private int staffId = -1;
	private String username;	
	
	private boolean showForm = true;

    public boolean isShowForm() {
        return showForm;
    }
    
    //CONSTRUCTOR: Add some contact data from db to app 
    public ContactApplication() throws Exception {
        contacts = new ArrayList<>();
        //FOR inheritance mapping.
        conDomestics = new ArrayList<ConDomestic>();
        conInternationals = new ArrayList<ConInternational>();

        //instantiate contactManagedBean
        ELContext elContext = FacesContext.getCurrentInstance().getELContext();
        contactManagedBean = (ContactManagedBean) FacesContext.getCurrentInstance().getApplication()
                .getELResolver().getValue(elContext, null, "contactManagedBean");

        staffManagedBean = (StaffManagedBean) FacesContext.getCurrentInstance().getApplication()
                .getELResolver().getValue(elContext, null, "staffManagedBean");
        
        //get username
//        username = FacesContext.getCurrentInstance().getExternalContext().getUserPrincipal().getName();
//        
//        if (!username.isEmpty() && !username.equals("null") && !username.equals(""))
//        	setStaffId(username);
        
        
        //get contacts from db 
        updateContactList();
        //FOR inheritance mapping.
        updateConDomesticList();
        updateConInternationalList();
    }

	public ArrayList<Contact> getContacts() {
		return contacts;
	}

	public void setContacts(ArrayList<Contact> contacts) {
		this.contacts = contacts;
	}
    
	public void updateContactList() {
		//get username
        username = FacesContext.getCurrentInstance().getExternalContext().getUserPrincipal().getName();
        
        if (!username.isEmpty() && !username.equals("null") && !username.equals(""))
        	setStaffId(username);
		
		contacts.clear();
		for (Contact contact: contactManagedBean.getAllContacts()) {
			if (staffId==-1 || staffId == 1)
				contacts.add(contact);
			else {
				int id = contact.getCustomer().getStaff().getId();
				if (id==staffId) {
					contacts.add(contact);
				}
			}
		}
		setContacts(contacts);
	}
	
//	------------------------------------------------------------
	//FOR inheritance mapping. (getter / setter)
	public ArrayList<ConDomestic> getConDomestics() {
		return conDomestics;
	}

	public void setConDomestics(ArrayList<ConDomestic> conDomestics) {
		this.conDomestics = conDomestics;
	}

	public ArrayList<ConInternational> getConInternationals() {
		return conInternationals;
	}

	public void setConInternationals(ArrayList<ConInternational> conInternationals) {
		this.conInternationals = conInternationals;
	}

	//FOR inheritance mapping.
	public void updateConDomesticList() {
		conDomestics.clear();
		for (ConDomestic conDomestic: contactManagedBean.getAllConDomestics()) {
//			conDomestics.add(conDomestic);
			if (staffId==-1 || staffId == 1)
				conDomestics.add(conDomestic);
			else {
				int id = conDomestic.getCustomer().getStaff().getId();
				if (id==staffId) {
					conDomestics.add(conDomestic);
				}
			}			
		}
		setConDomestics(conDomestics);
	}
	
	//FOR inheritance mapping.
	public void updateConInternationalList() {
		conInternationals.clear();
		for (ConInternational conInternational: contactManagedBean.getAllConInternationals()) {
//			conInternationals.add(conInternational);
			if (staffId==-1 || staffId == 1)
				conInternationals.add(conInternational);
			else {
				int id = conInternational.getCustomer().getStaff().getId();
				if (id==staffId) {
					conInternationals.add(conInternational);
				}
			}	
		}
		setConInternationals(conInternationals);
	}
//	---------------------------------------------------------
	
	
	public void searchContactById (int contactId) {
//		contacts.clear();
		conDomestics.clear();
		conInternationals.clear();
		
		Contact contact = contactManagedBean.searchContactById(contactId);
		if (contact != null) {
//			if (contact instanceof ConDomestic)
//				conDomestics.add((ConDomestic) contact);
//			else
//				conInternationals.add((ConInternational) contact);
			
			if (staffId==-1 || staffId == 1) {
				if (contact instanceof ConDomestic)
					conDomestics.add((ConDomestic) contact);
				else
					conInternationals.add((ConInternational) contact);
			}
			else {
				int id = contact.getCustomer().getStaff().getId();
				if (id==staffId) {
					if (contact instanceof ConDomestic)
						conDomestics.add((ConDomestic) contact);
					else
						conInternationals.add((ConInternational) contact);
				}
			}
		}
	}

	public void searchContactByCustomerId(int customerId) {
		contacts.clear();
		conDomestics.clear();
		conInternationals.clear();
		
		Set<Contact> contacts = contactManagedBean.searchContactByCustomerId(customerId);
		if (contacts != null) {
			for (Contact contact: contacts) {
//				if (contact instanceof ConDomestic)
//					conDomestics.add((ConDomestic) contact);
//				else
//					conInternationals.add((ConInternational) contact);
				
				if (staffId==-1 || staffId == 1) {
					contacts.add(contact);
					if (contact instanceof ConDomestic)
						conDomestics.add((ConDomestic) contact);
					else
						conInternationals.add((ConInternational) contact);
				}
				else {
					int id = contact.getCustomer().getStaff().getId();
					if (id==staffId) {
						contacts.add(contact);
						if (contact instanceof ConDomestic)
							conDomestics.add((ConDomestic) contact);
						else
							conInternationals.add((ConInternational) contact);
					}
				}
			}			
		}		
	}

	public void searchContactsByPositionAndPreference(String position, String preference) {
//		contacts.clear();
		conDomestics.clear();
		conInternationals.clear();
		
		List<Contact> contacts = contactManagedBean.searchContactsByPositionAndPreference(position, preference);
		if (contacts != null) {
			for (Contact contact: contacts) {
//				if (contact instanceof ConDomestic)
//					conDomestics.add((ConDomestic) contact);
//				else
//					conInternationals.add((ConInternational) contact);
				
				if (staffId==-1 || staffId == 1) {
					if (contact instanceof ConDomestic)
						conDomestics.add((ConDomestic) contact);
					else
						conInternationals.add((ConInternational) contact);
				}
				else {
					int id = contact.getCustomer().getStaff().getId();
					if (id==staffId) {
						if (contact instanceof ConDomestic)
							conDomestics.add((ConDomestic) contact);
						else
							conInternationals.add((ConInternational) contact);
					}
				}
				
			}			
		}		
	}
    
//  ------------------------
    
	public int getStaffId() {
		return staffId;
	}

	public void setStaffId(String username) {
		staffId = -1;
		staffId = staffManagedBean.searchIDByUsername(username);
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
	
//	-----------------------------
	
	public int countContact() {
		int count = contacts.size();
		return count;
	}
	
}
