package fit5042.assign.controller;

import java.io.Serializable;
import java.util.Set;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

import fit5042.assign.repository.entities.Customer;
import fit5042.assign.repository.entities.Industry;

@RequestScoped
@Named(value = "industryBean")
public class IndustryBean implements Serializable{

	private int industryId;
	private String industryType;
	
//	private Set<Customer> customers;
	
	private Set<Industry> industries;

	public IndustryBean() {
		super();
	}

	public IndustryBean(int industryId, String industryType, Set<Industry> industries) {
		super();
		this.industryId = industryId;
		this.industryType = industryType;
		this.industries = industries;
	}

	public int getIndustryId() {
		return industryId;
	}

	public void setIndustryId(int industryId) {
		this.industryId = industryId;
	}

	public String getIndustryType() {
		return industryType;
	}

	public void setIndustryType(String industryType) {
		this.industryType = industryType;
	}

	public Set<Industry> getIndustries() {
		return industries;
	}

	public void setIndustries(Set<Industry> industries) {
		this.industries = industries;
	}
	
	
	
}
