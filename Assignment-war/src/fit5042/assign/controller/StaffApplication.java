package fit5042.assign.controller;

import java.util.ArrayList;

import javax.el.ELContext;
import javax.enterprise.context.ApplicationScoped;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import fit5042.assign.mbeans.StaffManagedBean;
import fit5042.assign.repository.entities.Staff;

@Named(value = "staffApplication")
@ApplicationScoped
public class StaffApplication {

	@ManagedProperty(value = "#{staffManagedBean}")
	StaffManagedBean staffManagedBean;
	
	private ArrayList<Staff> staffs;
	
	private int staffId = -1;
	private String username;
	
	private boolean showForm = true;

    public boolean isShowForm() {
        return showForm;
    }

	public StaffApplication() throws Exception{
		staffs = new ArrayList<>();
		
		//instantiate staffManagedBean
        ELContext elContext = FacesContext.getCurrentInstance().getELContext();
        staffManagedBean = (StaffManagedBean) FacesContext.getCurrentInstance().getApplication()
                .getELResolver().getValue(elContext, null, "staffManagedBean");
        
       //get username
//        username = FacesContext.getCurrentInstance().getExternalContext().getUserPrincipal().getName();
//        
//        if (!username.isEmpty() && !username.equals("null") && !username.equals(""))
//        	setStaffId(username);
        
        //get staffs from db;
        updateStaffList();
        
	}

	public ArrayList<Staff> getStaffs() {
		return staffs;
	}

	public void setStaffs(ArrayList<Staff> staffs) {
		this.staffs = staffs;
	}
    
	public void updateStaffList() {
		//get username
        username = FacesContext.getCurrentInstance().getExternalContext().getUserPrincipal().getName();
        
        if (!username.isEmpty() && !username.equals("null") && !username.equals(""))
        	setStaffId(username);
        
		staffManagedBean.setDisableButton();
		
		staffs.clear();		
		for (Staff staff: staffManagedBean.getAllStaffs()) {
//			staffs.add(staff);
			if (staffId==-1 || staffId == 1)
				staffs.add(staff);
			else {
				int id = staff.getId();
				if (id==staffId)
					staffs.add(staff);
			}
		}
		setStaffs(staffs);		
	}
	
	public void searchStaffById (int staffId) {
		staffs.clear();
		
		Staff staff = staffManagedBean.searchStaffById(staffId);
		if (staff != null) {
//			staffs.add(staff);
			if (staffId==-1 || staffId == 1)
				staffs.add(staff);
			else {
				int id = staff.getId();
				if (id==staffId)
					staffs.add(staff);
			}
		}
	}
	
//  ------------------------
    
	public int getStaffId() {
		return staffId;
	}

	public void setStaffId(String username) {
		staffId = -1;
		staffId = staffManagedBean.searchIDByUsername(username);
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
	
}
