package fit5042.assign.controller;

import javax.el.ELContext;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedProperty;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import fit5042.assign.mbeans.CustomerManagedBean;
import fit5042.assign.mbeans.StaffManagedBean;
import fit5042.assign.repository.entities.Customer;

@RequestScoped
@Named("customerAdd")
public class CustomerAdd {
	
	@ManagedProperty(value = "#{customerManagedBean}")
	CustomerManagedBean customerManagedBean;
	
	@ManagedProperty(value = "#{staffManagedBean}")
	StaffManagedBean staffManagedBean;
	
	private int staffId = -1;
	private String username;	
	
	private boolean showForm = true;
	
//	private Customer customer;
	
	private CustomerBean customer;
	
	CustomerApplication app;
	
	private UIComponent component;
	
	public UIComponent getComponent() {
		return component;
	}

	public void setComponent(UIComponent component) {
		this.component = component;
	}
	
	public CustomerAdd() {
		//get application context bean
		ELContext context = FacesContext.getCurrentInstance().getELContext();
		app = (CustomerApplication) FacesContext.getCurrentInstance()
                .getApplication()
                .getELResolver()
                .getValue(context, null, "customerApplication");
		
		//instantiate customerManagedBean
        ELContext elContext = FacesContext.getCurrentInstance().getELContext();
        customerManagedBean = (CustomerManagedBean) FacesContext.getCurrentInstance().getApplication()
                .getELResolver().getValue(elContext, null, "customerManagedBean");
        
        staffManagedBean = (StaffManagedBean) FacesContext.getCurrentInstance().getApplication()
                .getELResolver().getValue(elContext, null, "staffManagedBean");
        
        //get username
        username = FacesContext.getCurrentInstance().getExternalContext().getUserPrincipal().getName();
        
        if (!username.isEmpty() && !username.equals("null") && !username.equals(""))
        	setStaffId(username);
		
	}
	
	public void customerAdd(CustomerBean localCustomer) {
		try {
//			localCustomer.setCustomerId(customerManagedBean.getFinalCustomerId()+1);
			
			if(staffId!=-1 && staffId!=1)
				localCustomer.setId(staffId);
			
			customerManagedBean.addCustomer(localCustomer);
			
			app.updateCustomerList();
			
			FacesContext.getCurrentInstance().addMessage(component.getClientId(), new FacesMessage("Customer Added!"));
			
		} catch(Exception e) {
			
		}
		
		showForm = true;
	}
	
	public void customerAddFirst() {
		try {
//			localCustomer.setCustomerId(customerManagedBean.getFinalCustomerId()+1);
			
			customerManagedBean.addCustomerFirst();
			
			app.updateCustomerList();
			
			FacesContext.getCurrentInstance().addMessage(component.getClientId(), new FacesMessage("Customer Added!"));
			
		} catch(Exception e) {
			
		}
		
		showForm = true;
	}
	
	public boolean isShowForm() {
        return showForm;
    }


//	public Customer getCustomer() {
//		return customer;
//	}
//
//	public void setCustomer(Customer customer) {
//		this.customer = customer;
//	}
	
	public CustomerBean getCustomer() {
		return customer;
	}

	public void setCustomer(CustomerBean customer) {
		this.customer = customer;
	}
	
//  ------------------------
    
	public int getStaffId() {
		return staffId;
	}

	public void setStaffId(String username) {
		staffId = -1;
		staffId = staffManagedBean.searchIDByUsername(username);
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
	
}
