package fit5042.assign.controller;

import javax.el.ELContext;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedProperty;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import fit5042.assign.mbeans.IndustryManagedBean;
import fit5042.assign.repository.entities.Industry;

@RequestScoped
@Named("industryRemove")
public class IndustryRemove {
	
	@ManagedProperty(value = "#{industryManagedBean}")
	IndustryManagedBean industryManagedBean;
	
	private boolean showForm = true;
	
	private Industry industry;
	
	IndustryApplication app;
	
	private UIComponent component;

	public IndustryRemove() {
		//get application context bean
		ELContext context = FacesContext.getCurrentInstance().getELContext();
		app = (IndustryApplication) FacesContext.getCurrentInstance()
                .getApplication()
                .getELResolver()
                .getValue(context, null, "industryApplication");
		
		app.updateIndustryList();
		
		//instantiate industryManagedBean
        ELContext elContext = FacesContext.getCurrentInstance().getELContext();
        industryManagedBean = (IndustryManagedBean) FacesContext.getCurrentInstance().getApplication()
                .getELResolver().getValue(elContext, null, "industryManagedBean");
	}

	public void industryRemove(int industryId) {
		try {
			//remove this contact from db via EJB
			industryManagedBean.removeIndustry(industryId);
			
			//refresh the list in Application bean
			app.updateIndustryList();
			
			//show confirmation message.
			FacesContext.getCurrentInstance().addMessage(component.getClientId(), new FacesMessage("Industries and its customers and contacts DELETED!!"));
			
		} catch (Exception e) {
			
		}		
		showForm = true;
	}
	
	public boolean isShowForm() {
        return showForm;
    }
	
	public Industry getIndustry() {
		return industry;
	}

	public void setIndustry(Industry industry) {
		this.industry = industry;
	}

	public UIComponent getComponent() {
		return component;
	}

	public void setComponent(UIComponent component) {
		this.component = component;
	}
	
	

}
