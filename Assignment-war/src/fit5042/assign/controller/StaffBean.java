package fit5042.assign.controller;

import java.io.Serializable;
import java.util.Set;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

import fit5042.assign.repository.entities.Staff;

@RequestScoped
@Named(value = "staffBean")
public class StaffBean implements Serializable{
	
	private int id;
	private String username;
	private String password;
	private String userType;
	
	private Set<Staff> staffs;

	public StaffBean() {
		super();
	}

	public StaffBean(int id, String username, String password, String userType, Set<Staff> staffs) {
		super();
		this.id = id;
		this.username = username;
		this.password = password;
		this.userType = userType;
		this.staffs = staffs;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public Set<Staff> getStaffs() {
		return staffs;
	}

	public void setStaffs(Set<Staff> staffs) {
		this.staffs = staffs;
	}
		
}
