package fit5042.assign.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.el.ELContext;
import javax.enterprise.context.ApplicationScoped;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import fit5042.assign.mbeans.CustomerManagedBean;
import fit5042.assign.mbeans.StaffManagedBean;
import fit5042.assign.repository.entities.Customer;

@Named(value = "customerApplication")
@ApplicationScoped
public class CustomerApplication {

	@ManagedProperty(value = "#{customerManagedBean}")
	CustomerManagedBean customerManagedBean;
	
	@ManagedProperty(value = "#{staffManagedBean}")
	StaffManagedBean staffManagedBean;
	
	private ArrayList<Customer> customers;
	
	private int staffId = -1;
	private String username;
	
	private boolean showForm = true;

    public boolean isShowForm() {
        return showForm;
    }
    
    //CONSTRUCTOR: Add some customer data from db to app 
    public CustomerApplication() throws Exception {
        customers = new ArrayList<>();

        //instantiate customerManagedBean
        ELContext elContext = FacesContext.getCurrentInstance().getELContext();
        customerManagedBean = (CustomerManagedBean) FacesContext.getCurrentInstance().getApplication()
                .getELResolver().getValue(elContext, null, "customerManagedBean");
        
        staffManagedBean = (StaffManagedBean) FacesContext.getCurrentInstance().getApplication()
                .getELResolver().getValue(elContext, null, "staffManagedBean");

        //get username
//        username = FacesContext.getCurrentInstance().getExternalContext().getUserPrincipal().getName();
//        
//        if (!username.isEmpty() && !username.equals("null") && !username.equals(""))
//        	setStaffId(username);
        	
        //get customers from db
    	updateCustomerList();
    }
    
    
	public ArrayList<Customer> getCustomers() {
		//updateCustomerList();
		
		return customers;
	}

	public void setCustomers(ArrayList<Customer> customers) {
		this.customers = customers;
	}
    
	//when loading, and after adding or deleting, the customer list needs to be refreshed
    //this method is for that purpose
    public void updateCustomerList() {
    	//get username
        username = FacesContext.getCurrentInstance().getExternalContext().getUserPrincipal().getName();
        setUsername(username);
        if (!username.isEmpty() && !username.equals("null") && !username.equals(""))
        	setStaffId(username);
        
		customers.clear();
		for (Customer customer: customerManagedBean.getAllCustomers()) {
			if (staffId==-1 || staffId == 1)
				customers.add(customer);
			else {
				int id = customer.getStaff().getId();
				if (id==staffId)
					customers.add(customer);
			}
		}
		setCustomers(customers);
    }

	public void searchCustomerById(int customerId) {
		customers.clear();
		
		Customer customer = customerManagedBean.searchCustomerById(customerId);
		if (customer != null) {
//			customers.add(customer);
			if (staffId==-1 || staffId == 1)
				customers.add(customer);
			else {
				int id = customer.getStaff().getId();
				if (id==staffId)
					customers.add(customer);
			}
		}
		
	}
    
    public void searchCustomerByContactId (int contactId) {
    	customers.clear();
		Customer customer = customerManagedBean.searchCustomerByContactId(contactId);
		if (customer != null) {
//			customers.add(customer);
			if (staffId==-1 || staffId == 1)
				customers.add(customer);
			else {
				int id = customer.getStaff().getId();
				if (id==staffId)
					customers.add(customer);
			}
		}		
    }

	public void searchCustomersByYearAndIndustry(int yearRegistered, int industryId) {
		customers.clear();
		List<Customer> customers = customerManagedBean.searchCustomersByYearAndIndustry(yearRegistered, industryId);
		for (Customer customer: customers) {
//			this.customers.add(customer);
			if (staffId==-1 || staffId == 1)
				this.customers.add(customer);
			else {
				int id = customer.getStaff().getId();
				if (id==staffId)
					this.customers.add(customer);
			}
		}			
	}


	public void searchCustomerByStaffId(int staffId2) {
		customers.clear();
		List<Customer> customers = customerManagedBean.searchCustomerByStaffId(staffId2);
		for (Customer customer: customers) {
//			this.customers.add(customer);
			if (staffId==-1 || staffId == 1)
				this.customers.add(customer);
			else {
				int id = customer.getStaff().getId();
				if (id==staffId)
					this.customers.add(customer);
			}
		}		
	}
    
//    ------------------------
    
	public int getStaffId() {
		return staffId;
	}

	public void setStaffId(String username) {
		staffId = -1;
		staffId = staffManagedBean.searchIDByUsername(username);
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

//	-------------------------------------------

	
}
