package fit5042.assign.controller;

import javax.el.ELContext;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import fit5042.assign.repository.entities.Contact;

@Named(value = "contactController")
@RequestScoped
public class ContactController {

	private int contactID; //the contact index

	public int getContactID() {
		return contactID;
	}

	public void setContactID(int contactID) {
		this.contactID = contactID;
	}
	
	private Contact contact;

	public ContactController() {
		//Assign contactID via GET parameter
		contactID = Integer.valueOf(FacesContext.getCurrentInstance()
                .getExternalContext()
                .getRequestParameterMap()
                .get("contactID"));
		//assign contact based on the contactID.
		contact = getContact();
	}

	public Contact getContact() {
		if (contact == null) {
			//get application context bean
			ELContext context = FacesContext.getCurrentInstance().getELContext();
			ContactApplication app
            = (ContactApplication) FacesContext.getCurrentInstance()
                    .getApplication()
                    .getELResolver()
                    .getValue(context, null, "contactApplication");
			//minus 1 in contactID since it added i in (contactlist.xhtml)
			return app.getContacts().get(--contactID);
		}
		return contact;
	}

	public void setContact(Contact contact) {
		this.contact = contact;
	}
	
	
	
}
