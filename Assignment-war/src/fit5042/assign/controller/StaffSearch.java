package fit5042.assign.controller;

import javax.el.ELContext;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import fit5042.assign.repository.entities.Staff;

@Named(value = "staffSearch")
@RequestScoped
public class StaffSearch {
	
	private boolean showForm = true;
	
	private Staff staff;
	
	StaffApplication app;

	public StaffSearch() {
		//get application context bean
		ELContext context = FacesContext.getCurrentInstance().getELContext();
		app = (StaffApplication) FacesContext.getCurrentInstance()
                .getApplication()
                .getELResolver()
                .getValue(context, null, "staffApplication");
		
		app.updateStaffList();
	}

	public StaffApplication getApp() {
		return app;
	}

	public void setApp(StaffApplication app) {
		this.app = app;
	}
	
	public void searchAll() {
		try {
            //return all properties from db via EJB
            app.updateStaffList();
        } catch (Exception ex) {

        }
        showForm = true;
	}

//	---------------------------------------------------------
	
	private int staffId;

	public int getStaffId() {
		return staffId;
	}

	public void setStaffId(int staffId) {
		this.staffId = staffId;
	}
	
	public void searchStaffById(int staffId) {
		try {
			app.searchStaffById(staffId);
		} catch (Exception e) {
			
		}
		showForm = true;
	}
	
}
