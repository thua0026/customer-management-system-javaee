package fit5042.assign.controller;

import javax.el.ELContext;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedProperty;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import fit5042.assign.mbeans.IndustryManagedBean;

@RequestScoped
@Named("industryAdd")
public class IndustryAdd {

	@ManagedProperty(value = "#{industryManagedBean}")
	IndustryManagedBean industryManagedBean;
	
	private boolean showForm = true;
	
	private IndustryBean industry;
	
	IndustryApplication app;
	
	private UIComponent component;
	
	public IndustryAdd() {
		//get application context bean
		ELContext context = FacesContext.getCurrentInstance().getELContext();
		app = (IndustryApplication) FacesContext.getCurrentInstance()
                .getApplication()
                .getELResolver()
                .getValue(context, null, "industryApplication");
		
		//instantiate industryManagedBean
        ELContext elContext = FacesContext.getCurrentInstance().getELContext();
        industryManagedBean = (IndustryManagedBean) FacesContext.getCurrentInstance().getApplication()
                .getELResolver().getValue(elContext, null, "industryManagedBean");
	}
	
	public UIComponent getComponent() {
		return component;
	}

	public void setComponent(UIComponent component) {
		this.component = component;
	}
	
	public void industryAdd(IndustryBean localIndustry) {
		try {
//			localContact.setContactId(contactManagedBean.getFinalContactId()+1);
			
			industryManagedBean.addIndustry(localIndustry);
			
			app.updateIndustryList();
			
			FacesContext.getCurrentInstance().addMessage(component.getClientId(), new FacesMessage("Industry Added!"));
			
		} catch(Exception e) {
			
		}
		
		showForm = true;
	}
	
	public void industryAddFirst() {
		try {
	//		localContact.setContactId(contactManagedBean.getFinalContactId()+1);
			
			industryManagedBean.addIndustryFirst();
			
			app.updateIndustryList();
			
			FacesContext.getCurrentInstance().addMessage(component.getClientId(), new FacesMessage("Industry Added!"));
			
		} catch(Exception e) {
			
		}
		
		showForm = true;
	}
	
	public boolean isShowForm() {
        return showForm;
    }

	public IndustryBean getIndustry() {
		return industry;
	}

	public void setIndustry(IndustryBean industry) {
		this.industry = industry;
	}
	
		
}
