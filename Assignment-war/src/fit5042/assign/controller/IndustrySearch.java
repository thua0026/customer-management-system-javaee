package fit5042.assign.controller;

import javax.el.ELContext;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import fit5042.assign.repository.entities.Industry;

@Named(value = "industrySearch")
@RequestScoped
public class IndustrySearch {

	private boolean showForm = true;
	
	private Industry industry;
	
	IndustryApplication app;

	public IndustrySearch() {
		//get application context bean
		ELContext context = FacesContext.getCurrentInstance().getELContext();
		app = (IndustryApplication) FacesContext.getCurrentInstance()
                .getApplication()
                .getELResolver()
                .getValue(context, null, "industryApplication");
		
		app.updateIndustryList();
	}

	public IndustryApplication getApp() {
		return app;
	}

	public void setApp(IndustryApplication app) {
		this.app = app;
	}
	
	public void searchAll() {
		try {
            //return all properties from db via EJB
            app.updateIndustryList();
        } catch (Exception ex) {

        }
        showForm = true;
	}
	
//	---------------------------------------------
	
	private int industryId;

	public int getIndustryId() {
		return industryId;
	}

	public void setIndustryId(int industryId) {
		this.industryId = industryId;
	}
	
	public void searchIndustryById (int industryId) {
		try {
			app.searchIndustryById(industryId);
		} catch (Exception e) {
			
		}
		showForm = true;
	}
	
		
}
