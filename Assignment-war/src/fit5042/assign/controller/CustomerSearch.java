package fit5042.assign.controller;

import javax.el.ELContext;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import fit5042.assign.repository.entities.Customer;

@Named(value = "customerSearch")
@RequestScoped
public class CustomerSearch {

	private boolean showForm = true;
	
	private Customer customer;
	
	CustomerApplication app;
	
	public CustomerSearch() {
		//get application context bean
		ELContext context = FacesContext.getCurrentInstance().getELContext();
		app = (CustomerApplication) FacesContext.getCurrentInstance()
                .getApplication()
                .getELResolver()
                .getValue(context, null, "customerApplication");
		
		app.updateCustomerList();
	}

	public CustomerApplication getApp() {
		return app;
	}

	public void setApp(CustomerApplication app) {
		this.app = app;
	}
	
	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public void searchAll() {
		try {
            //return all properties from db via EJB
            app.updateCustomerList();
        } catch (Exception ex) {

        }
        showForm = true;
	}
	
//	-------------------------------------------------
	private int searchByInt;
//    private double searchByDouble;

	public int getSearchByInt() {
		return searchByInt;
	}

	public void setSearchByInt(int searchByInt) {
		this.searchByInt = searchByInt;
	}
	
	
	public void searchCustomerById(int customerId) {
		try {
			app.searchCustomerById(customerId);
		} catch (Exception e) {
			
		}
		showForm = true;
	}
	
//	-------------------------------------------------
	
//	private int contactId;
//
//	public int getContactId() {
//		return contactId;
//	}
//
//	public void setContactId(int contactId) {
//		this.contactId = contactId;
//	}
	
	public void searchCustomerByContactId(int contactId) {
		try {
        	int p = contactId;
            //search all customer by contact from db via EJB 
            app.searchCustomerByContactId(contactId);
        } catch (Exception ex) {

        }
        showForm = true;
	}
	
//	---------------------------------------------------
	
	private int yearRegistered;

	public int getYearRegistered() {
		return yearRegistered;
	}

	public void setYearRegistered(int yearRegistered) {
		this.yearRegistered = yearRegistered;
	}
	
	public void searchCustomerByYearAndIndustry(int yearRegistered, int industryId) {
		try {
			app.searchCustomersByYearAndIndustry(yearRegistered, industryId);			
		}catch (Exception ex) {
			
		}
		showForm = true;
	}
	
//	-------------------------------------------------------
	
	public void searchCustomerByStaffId(int staffId) {
		try {
        	int p = staffId;
            //search all customer by contact from db via EJB 
            app.searchCustomerByStaffId(staffId);
        } catch (Exception ex) {

        }
        showForm = true;
	}
	
}
