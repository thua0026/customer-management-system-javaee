package fit5042.assign.controller;

import java.util.ArrayList;
import java.util.List;

import javax.el.ELContext;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import fit5042.assign.mbeans.CustomerManagedBean;
import fit5042.assign.repository.entities.Customer;
import fit5042.assign.repository.entities.Staff;

@Named(value = "staffController")
@RequestScoped
public class StaffController {

	private int staffID; //staff index

	public int getStaffID() {
		return staffID;
	}

	public void setStaffID(int staffID) {
		this.staffID = staffID;
	}

	private Staff staff;
	
	public StaffController() {
		//Assign contactID via GET parameter
		staffID = Integer.valueOf(FacesContext.getCurrentInstance()
                .getExternalContext()
                .getRequestParameterMap()
                .get("staffID"));
		//assign contact based on the contactID.
		staff = getStaff();
	}

	public Staff getStaff() {
		if (staff == null) {
			//get application context bean
			ELContext context = FacesContext.getCurrentInstance().getELContext();
			StaffApplication app = (StaffApplication) FacesContext.getCurrentInstance()
	                .getApplication()
	                .getELResolver()
	                .getValue(context, null, "staffApplication");
			//minus 1 in staffID
			return app.getStaffs().get(--staffID);
		}		
		return staff;
	}

	public void setStaff(Staff staff) {
		this.staff = staff;
	}

	public List<Customer> getCustomers(){
		int staffId = staff.getId();
//		List<Customer> outputs = new ArrayList<Customer>();
		
		//instantiate customerManagedBean
        ELContext elContext = FacesContext.getCurrentInstance().getELContext();
        CustomerManagedBean customerManagedBean = (CustomerManagedBean) FacesContext.getCurrentInstance().getApplication()
                .getELResolver().getValue(elContext, null, "customerManagedBean");
        
        
        List<Customer> customers = customerManagedBean.searchCustomerByStaffId(staffId);

        return customers;
	}
	
}
