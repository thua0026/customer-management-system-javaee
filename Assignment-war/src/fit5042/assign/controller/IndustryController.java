package fit5042.assign.controller;

import javax.el.ELContext;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import fit5042.assign.repository.entities.Industry;

@Named(value = "industryController")
@RequestScoped
public class IndustryController {

	private int industryID; //industry index

	public int getIndustryID() {
		return industryID;
	}

	public void setIndustryID(int industryID) {
		this.industryID = industryID;
	}
	
	private Industry industry;

	public IndustryController() {
		//Assign contactID via GET parameter
		industryID = Integer.valueOf(FacesContext.getCurrentInstance()
                .getExternalContext()
                .getRequestParameterMap()
                .get("industryID"));
		//assign contact based on the contactID.
		industry = getIndustry();
	}

	public Industry getIndustry() {
		if (industry == null) {
			//get application context bean
			ELContext context = FacesContext.getCurrentInstance().getELContext();
			IndustryApplication 
			app = (IndustryApplication) FacesContext.getCurrentInstance()
	                .getApplication()
	                .getELResolver()
	                .getValue(context, null, "industryApplication");
			//minus 1 in industryID since it added i in (industrylist.xhtml)
			return app.getIndustries().get(--industryID);
		}
		return industry;
	}

	public void setIndustry(Industry industry) {
		this.industry = industry;
	}
	
	
	
}
