package fit5042.assign.controller;

import javax.el.ELContext;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import fit5042.assign.repository.entities.ConInternational;
import fit5042.assign.repository.entities.Contact;

@Named(value = "conInternationalController")
@RequestScoped
public class ConInternationalController {

	private int contactID; //the contact index

	public int getContactID() {
		return contactID;
	}

	public void setContactID(int contactID) {
		this.contactID = contactID;
	}
	
	private ConInternational contact;

	public ConInternationalController() {
		//Assign contactID via GET parameter
		contactID = Integer.valueOf(FacesContext.getCurrentInstance()
                .getExternalContext()
                .getRequestParameterMap()
                .get("contactID"));
		//assign contact based on the contactID.
		contact = getContact();
	}

	public ConInternational getContact() {
		if (contact == null) {
			//get application context bean
			ELContext context = FacesContext.getCurrentInstance().getELContext();
			ContactApplication app
            = (ContactApplication) FacesContext.getCurrentInstance()
                    .getApplication()
                    .getELResolver()
                    .getValue(context, null, "contactApplication");
			//minus 1 in contactID since it added i in (contactlist.xhtml)
			return app.getConInternationals().get(--contactID);
		}
		return contact;
	}

	public void setContact(ConInternational contact) {
		this.contact = contact;
	}

	
}
