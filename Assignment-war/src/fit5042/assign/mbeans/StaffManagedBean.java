package fit5042.assign.mbeans;

import java.io.Serializable;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;

import fit5042.assign.controller.StaffBean;
import fit5042.assign.repository.GroupsRepository;
import fit5042.assign.repository.StaffRepository;
import fit5042.assign.repository.entities.Groups;
import fit5042.assign.repository.entities.Staff;

@ManagedBean(name="staffManagedBean")
@SessionScoped
public class StaffManagedBean implements Serializable {
	
	@EJB
	StaffRepository staffRepository;
	
	@EJB
	GroupsRepository groupsRepository;
	
	private UIComponent component;
	
	private boolean showButton = true;
	
    public boolean isShowButton() {
		return showButton;
	}

	public void setShowButton(boolean showButton) {
		this.showButton = showButton;
	}
	
	private boolean disableButton = false;

	public boolean isDisableButton() {
		return disableButton;
	}

	public void setDisableButton() {
		this.disableButton = false;
	}

	public UIComponent getComponent() {
        return component;
    }

    public void setComponent(UIComponent component) {
        this.component = component;
    }

	public StaffManagedBean() {
		super();
		disableButton = false;
	}
    
	public void addStaff(Staff staff) {
		try {
			//HASH the password
			String password = staff.getPassword();
			password = convertToHash(password);
			staff.setPassword(password);
			
			staffRepository.addStaff(staff);
			
			//add the staff to the group;
//			int id = staff.getId();
			String userName = staff.getUsername();
			Groups groups = new Groups();
			groups.setGroupname("normal");
//			groups.setId(id);
			groups.setUsername(userName);
			groupsRepository.addGroups(groups);
			
			showButton = false;
			disableButton = true;
			
		} catch (Exception e) {
			Logger.getLogger(StaffManagedBean.class.getName()).log(Level.SEVERE, null, e);
		}
	}
	
	public void addStaffFirst() {
		Staff staff = getAllStaffs().get(0);
		addStaff(staff);
	}
	
	public Staff searchStaffById(int id) {
		try {
			return staffRepository.searchStaffById(id);
		} catch (Exception e) {
			Logger.getLogger(StaffManagedBean.class.getName()).log(Level.SEVERE, null, e);
		}
		return null;
	}
	
	public int searchIDByUsername(String username) {
		try {
			List<Staff> staffs = staffRepository.getAllStaffs();
			for (Staff staff: staffs) {
				if(username.equals(staff.getUsername())){
					int staffId = staff.getId();
					return staffId;
				}
			}			
		} catch (Exception e) {
			Logger.getLogger(StaffManagedBean.class.getName()).log(Level.SEVERE, null, e);
		}
		return -1;
	}
	
	public List<Staff> getAllStaffs() {
		try {
			List<Staff> staffs = staffRepository.getAllStaffs();
			List<Staff> output = new ArrayList<>();
			//return normal user only.
			for(Staff staffElement : staffs) {
				if (staffElement.getUserType().toUpperCase().equals("NORMAL"))
					output.add(staffElement);
			}
			return output;
		} catch (Exception e) {
			Logger.getLogger(StaffManagedBean.class.getName()).log(Level.SEVERE, null, e);
		}
		return null;
	}
	
	public void removeStaff(int id) {
		try {
			staffRepository.removeStaff(id);
			//remove  the relevant group record.
			groupsRepository.removeGroups(id);
		} catch (Exception e) {
			Logger.getLogger(StaffManagedBean.class.getName()).log(Level.SEVERE, null, e);
		}
		
	}
	
	public void editStaff(Staff staff) {
		try {
			//check username
			int check = checkUsername(staff.getUsername(), staff.getId());
			if(check!=1 && check!=2) {
				//HASH the password
				String password = staff.getPassword();
				password = convertToHash(password);
				//compare the password
				int id = staff.getId();
				String passwordOrg = searchStaffById(id).getPassword();
				if(passwordOrg.equals(password)) {
					FacesContext.getCurrentInstance().addMessage(component.getClientId(), new FacesMessage("UPDATE FAILED!! NEW password must be differernt from the old one!!"));
				}else {
					//update staff vaule
					staff.setPassword(password);
					staffRepository.editStaff(staff);
					
					//edit the relevant groups (username);
					String userName = staff.getUsername();
//					int id = staff.getId();
					Groups groups = new Groups();
					groups.setId(id);
					groups.setGroupname("normal");
					groups.setUsername(userName);
					groupsRepository.editGroups(groups);

					FacesContext.getCurrentInstance().addMessage(component.getClientId(), new FacesMessage("Staff Updated !! Please click CLOSE button!!"));
					
					showButton = false;
					disableButton = true;
				}
			}
			
		} catch (Exception e) {
			Logger.getLogger(StaffManagedBean.class.getName()).log(Level.SEVERE, null, e);
		}
		
	}
	
//	-----------------------------------------------------
	private Staff covertStaffBeanToEntity(StaffBean localStaff) {
		Staff staff = new Staff();
		String username = localStaff.getUsername();
		String password = localStaff.getPassword();
		staff.setUsername(username);
		staff.setPassword(password);
		staff.setUserType("NORMAL");
		
		staff.setCustomers(null);
		
		return staff;		
	}
    
	public void addStaff(StaffBean localStaff) {
		Staff staff = covertStaffBeanToEntity(localStaff);
		addStaff(staff);
	}
	
	
//	-----------------------------------------------------------
	
	public String convertToHash(String passwordStr) throws NoSuchAlgorithmException {
		
//		https://stackoverflow.com/questions/3103652/hash-string-via-sha-256-in-java?rq=1
		MessageDigest md = MessageDigest.getInstance("SHA-256");

	    md.update(passwordStr.getBytes(StandardCharsets.UTF_8));
	    byte[] digest = md.digest();

	    String hex = String.format("%064x", new BigInteger(1, digest));
//	    System.out.println(hex);
		return hex;
	}
	
//	---------------------------------------------------
	
	public int checkUsername(String username, int id) {
		int check = 0;
//		String username = localStaff.getUsername();
		
		if(username.toLowerCase().equals("null")) {
			check = 1;
			if (id!=-1)
				FacesContext.getCurrentInstance().addMessage(component.getClientId(), new FacesMessage("Username cannot be null !!"));
			return check;
		}
		
		List<Staff> staffs = getAllStaffs();
		for (Staff staff: staffs) {
			String username1 = staff.getUsername();
			int id2 = staff.getId();
			if(username.equals(username1) && id!=id2) {
				check = 2;
				if(id!=-1)
					FacesContext.getCurrentInstance().addMessage(component.getClientId(), new FacesMessage("Username exist!! please enter another username"));
				return check;
			}
				
		}
		return check;
	}
	
	
	
}
