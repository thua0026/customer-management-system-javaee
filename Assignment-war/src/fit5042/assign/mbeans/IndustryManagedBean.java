package fit5042.assign.mbeans;

import java.io.Serializable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;

import fit5042.assign.controller.IndustryBean;
import fit5042.assign.repository.IndustryRepository;
import fit5042.assign.repository.entities.Industry;

@ManagedBean(name="industryManagedBean")
@SessionScoped
public class IndustryManagedBean implements Serializable{

	@EJB
	IndustryRepository industryRepository;
	
	private UIComponent component;
	
	public UIComponent getComponent() {
		return component;
	}

	public void setComponent(UIComponent component) {
		this.component = component;
	}

	public IndustryManagedBean() {
		super();
	}
	
	public void addIndustry(Industry industry) {
		try {
			industryRepository.addIndustry(industry);
		} catch (Exception e) {
			Logger.getLogger(IndustryManagedBean.class.getName()).log(Level.SEVERE, null, e);
		}
	}
	
	public void addIndustryFirst() {
		Industry industry = getAllIndustries().get(0);
		addIndustry(industry);
	}
	
	public Industry searchIndustryById(int industryId) {
		try {
			industryRepository.searchIndustryById(industryId);
		} catch (Exception e) {
			Logger.getLogger(IndustryManagedBean.class.getName()).log(Level.SEVERE, null, e);
		}
		return null;
	}
	
	public List<Industry> getAllIndustries(){
		try {
			List<Industry> industries = industryRepository.getAllIndustries();
			return industries;
		} catch (Exception e) {
			Logger.getLogger(IndustryManagedBean.class.getName()).log(Level.SEVERE, null, e);
		}
		return null;
	}
	
	public void removeIndustry(int industryId) {
		try {
			industryRepository.removeIndustry(industryId);
		} catch (Exception e) {
			Logger.getLogger(IndustryManagedBean.class.getName()).log(Level.SEVERE, null, e);
		}
		
	}
	
	public void editIndustry(Industry industry) {
		try {
			industryRepository.editIndustry(industry);
			FacesContext.getCurrentInstance().addMessage(component.getClientId(), new FacesMessage("Industry Updated !!"));
		} catch (Exception e) {
			Logger.getLogger(IndustryManagedBean.class.getName()).log(Level.SEVERE, null, e);
		}
		
	}
	
//	------------------------------------------------------
	private Industry convertIndustryBeanToEntity(IndustryBean localIndustry) {
		Industry industry = new Industry();
		String industryType = localIndustry.getIndustryType();
		industry.setIndustryType(industryType);
		
		industry.setCustomers(null);
		
		return industry;		
	}
	
	public void addIndustry(IndustryBean localIndustry) {
		Industry industry = convertIndustryBeanToEntity(localIndustry);
		addIndustry(industry);
	}
	
}
