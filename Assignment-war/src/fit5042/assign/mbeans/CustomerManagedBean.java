package fit5042.assign.mbeans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;

import fit5042.assign.controller.CustomerBean;
import fit5042.assign.repository.CustomerRepository;
import fit5042.assign.repository.IndustryRepository;
import fit5042.assign.repository.StaffRepository;
import fit5042.assign.repository.entities.Address;
import fit5042.assign.repository.entities.Contact;
import fit5042.assign.repository.entities.Customer;
import fit5042.assign.repository.entities.Industry;
import fit5042.assign.repository.entities.Staff;
import jdk.internal.org.objectweb.asm.util.CheckAnnotationAdapter;

@ManagedBean(name="customerManagedBean")
@SessionScoped
public class CustomerManagedBean implements Serializable {

	@EJB
	CustomerRepository customerRepository;
	
	@EJB
	StaffRepository staffRepository;
	
	@EJB
	IndustryRepository industryRepository;
	
	private UIComponent component;
	
	public UIComponent getComponent() {
		return component;
	}

	public void setComponent(UIComponent component) {
		this.component = component;
	}

	public CustomerManagedBean() {
	}
	
	public void addCustomer(Customer customer) {
		try {
			customerRepository.addCustomer(customer);
		} catch (Exception e) {
			Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, e);
			//
		}
	}

	public void addCustomerFirst() {
		Customer customer = getAllCustomers().get(0);
		customer.setContacts(null);
		addCustomer(customer);
	}

	public Customer searchCustomerById(int customerId) {
		try {
			return customerRepository.searchCustomerById(customerId);
		} catch (Exception e) {
			Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, e);
		}
		return null;
	}
	
	public Customer searchCustomerByContactId (int contactId){
		try {
            //retrieve contact by id
            for (Contact contact : customerRepository.getAllContacts()) {
                if (contact.getContactId() == contactId) {
                    return customerRepository.searchCustomerByContact(contact);
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
	}

	public List<Customer> getAllCustomers()  {
		try {
			List<Customer> customers = customerRepository.getAllCustomers();
			return customers;
		} catch (Exception e) {
			Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, e);
		}
		return null;
	}


	public void removeCustomer(int customerId)  {
		try {
			customerRepository.removeCustomer(customerId);
		} catch (Exception e) {
			Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, e);
		}
	}


	public void editCustomer(Customer customer) {
		try {
			Address address = customer.getAddress();
			String streetNo = address.getStreetNo();
			address.setStreetNo(streetNo);
			customer.setAddress(address);
			
//			String usernameString = customer.getStaff().getUsername();
			
			customerRepository.editCustomer(customer);
			FacesContext.getCurrentInstance().addMessage(component.getClientId(), new FacesMessage("Customer Updated !!"));
			
		} catch (Exception e) {
			Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, e);
		}
	}
	
//	---------------------------------------------------
	
	private Customer covertCustomerBeanToEntity(CustomerBean localCustomer) {
		Customer customer = new Customer();
		
		String streetNo = localCustomer.getStreetNo();
	    String streetName = localCustomer.getStreetName();
	    String district = localCustomer.getDistrict();
	    String zipcode = localCustomer.getZipcode();
	    String state = localCustomer.getState();
	    
	    Address address = new Address(streetNo, streetName, district, zipcode, state);
	    customer.setAddress(address);
	    
		String customerName = localCustomer.getCustomerName();
		int yearRegistered = localCustomer.getYearRegistered();
		int quantityOrdered = localCustomer.getQuantityOrdered();
		int noOfEmployee = localCustomer.getNoOfEmployee();
		String creditRating = localCustomer.getCreditRating();
		
		customer.setCustomerName(customerName);
		customer.setYearRegistered(yearRegistered);
		customer.setQuantityOrdered(quantityOrdered);
		customer.setNoOfEmployee(noOfEmployee);
		customer.setCreditRating(creditRating);
		
		int staffId = localCustomer.getId();
		Staff staff = new Staff();
		try {
			for (Staff staffElement : staffRepository.getAllStaffs()) {
			    if (staffElement.getId() == staffId) {
			        staff = staffElement;
			    }
			}
		} catch (Exception e) {
			Logger.getLogger(ContactManagedBean.class.getName()).log(Level.SEVERE, null, e);
		}
		customer.setStaff(staff);
		
		int industryId = localCustomer.getIndustryId(); 
		Industry industry = new Industry();
		try {
			for (Industry industryElement : industryRepository.getAllIndustries()) {
			    if (industryElement.getIndustryId() == industryId) {
			        industry = industryElement;
			    }
			}
		} catch (Exception e) {
			Logger.getLogger(ContactManagedBean.class.getName()).log(Level.SEVERE, null, e);
		}
		customer.setIndustry(industry);
		
		//Contact Must be set, it cannot be copied from any existing contact.
		customer.setContacts(null);
		
		return customer;		
	}
	
	public void addCustomer(CustomerBean localCustomer) {
		Customer customer = covertCustomerBeanToEntity(localCustomer);
		addCustomer(customer);		
	}
	
//	----------------------------------------------------------------------

	public List<Customer> searchCustomersByYearAndIndustry(int yearRegistered, int industryId) {
		List<Customer> output = new ArrayList<>();
		try {
            boolean contentCheck = false;
            for (Customer customer : customerRepository.getAllCustomers()) {
                int year = customer.getYearRegistered();
                int industry = customer.getIndustry().getIndustryId();
                if(yearRegistered!=0 && industryId!=0) {
                	contentCheck = true;
                	if(yearRegistered==year && industry==industryId)
                    	output.add(customer); 
                }else if(yearRegistered==0 && industryId!=0) {
                	contentCheck = true;
                	if(industry==industryId)
                    	output.add(customer);
                }else if(yearRegistered!=0 && industryId==0) {
                	contentCheck = true;
                	if(yearRegistered==year)
                    	output.add(customer);
                }                               	
            }            
//            if(!contentCheck) {
//            	FacesContext.getCurrentInstance().addMessage(component.getClientId(), new FacesMessage("NO INPUT, please select a industry and/or enter a year!!"));
//            }
        } catch (Exception ex) {
            Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
		return output;
	}
	
//	------------------------------------------------------------
	// de-assign a customer to a staff/user
	public void deassignStaff(int customerId) {
		Customer customer = searchCustomerById(customerId);
		try {
			Staff nullStaff = staffRepository.searchStaffById(0);
			customer.setStaff(nullStaff);
		} catch (Exception e) {
			e.printStackTrace();
		}
		editCustomer(customer);
	}
	
	//check whether to hide "de-assign staff" button
	public boolean checkStaff(int customerId) {
		Customer customer = searchCustomerById(customerId);
		boolean showButton = true;
		int id = customer.getStaff().getId();
		if(id==0)
			showButton = false;
		return showButton;
	}
	
	//show the de-assigned message
	public String displayDeassign(int customerId) {
		boolean showButton = checkStaff(customerId);
		if (!showButton)
			return "De-assigned";
		return "Assigned";
	}
	
//	-------------------------------------------------

	public List<Customer> searchCustomerByStaffId(int staffId2) {
		try {
			List<Customer> customers = getAllCustomers();
			List<Customer> outputs = new ArrayList<Customer>();
			for (Customer customer: customers) {
				if (customer.getStaff().getId()==staffId2)
					outputs.add(customer);
			}
			return outputs;
		}catch (Exception e) {

		}
		return null;
	}
	
}
