package fit5042.assign.mbeans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.validation.spi.ValidationProvider;

import fit5042.assign.controller.ContactBean;
import fit5042.assign.repository.ContactRepository;
import fit5042.assign.repository.entities.Address;
import fit5042.assign.repository.entities.ConDomestic;
import fit5042.assign.repository.entities.ConInternational;
import fit5042.assign.repository.entities.Contact;
import fit5042.assign.repository.entities.Customer;

@ManagedBean(name="contactManagedBean")
@SessionScoped
public class ContactManagedBean implements Serializable {

	@EJB
	ContactRepository contactRepository;

	private UIComponent component;

    public UIComponent getComponent() {
        return component;
    }

    public void setComponent(UIComponent component) {
        this.component = component;
    }
	
	public ContactManagedBean() {
	}
	
	public void addContact(Contact contact) {
		try {
			contactRepository.addContact(contact);
		} catch (Exception e) {
			Logger.getLogger(ContactManagedBean.class.getName()).log(Level.SEVERE, null, e);
		}
	}
	
	public void addContactFirst() {
		Contact contact = getAllContacts().get(0);
		addContact(contact);		
	}
	
	public Contact searchContactById(int contactId) {
		try {
			return contactRepository.searchContactById(contactId);
		} catch (Exception e) {
			Logger.getLogger(ContactManagedBean.class.getName()).log(Level.SEVERE, null, e);
		}
		return null;
	}
	
	public Set<Contact> searchContactByCustomerId(int customerId) {
		// TODO Auto-generated method stub
		try {
			for (Customer customer : contactRepository.getAllCustomers()) {
                if (customer.getCustomerId() == customerId) {
                    return contactRepository.searchContactByCustomer(customer);
                }
			}
		} catch (Exception e) {
			Logger.getLogger(ContactManagedBean.class.getName()).log(Level.SEVERE, null, e);
		}
		return null;
	}	
	
	public List<Contact> getAllContacts(){
		try {
			return contactRepository.getAllContacts();
		} catch (Exception e) {
			Logger.getLogger(ContactManagedBean.class.getName()).log(Level.SEVERE, null, e);
		}
		return null;
	}
	
	public void removeContact(int contactId) {
		try {
			contactRepository.removeContact(contactId);
		} catch (Exception e) {
			Logger.getLogger(ContactManagedBean.class.getName()).log(Level.SEVERE, null, e);
		}
	}
	
	public void editContact(Contact contact) {
		try {
			contactRepository.editContact(contact);
			FacesContext.getCurrentInstance().addMessage(component.getClientId(), new FacesMessage("Contact Updated !!"));
		} catch (Exception e) {
			Logger.getLogger(ContactManagedBean.class.getName()).log(Level.SEVERE, null, e);
		}
	}

	
//	-----------------------------------------------
	
	private Contact convertContactBeanToEntity(ContactBean localContact) {
		Contact contact = new Contact();
		String firstName = localContact.getFirstName();
		String lastName = localContact.getLastName();
		String position = localContact.getPosition();
		String email = localContact.getEmail();
		String phoneNo = localContact.getPhoneNo();
		String preference = localContact.getPreference();
		
		contact.setFirstName(firstName);
		contact.setLastName(lastName);
		contact.setPosition(position);
		contact.setEmail(email);
		contact.setPhoneNo(phoneNo);
		contact.setPreference(preference);
		
		int customerId = localContact.getCustomerId();
		Customer customer = new Customer();
		try {
			for (Customer customerElement : contactRepository.getAllCustomers()) {
			    if (customerElement.getCustomerId() == customerId) {
			        customer = customerElement;
			    }
			}
		} catch (Exception e) {
			Logger.getLogger(ContactManagedBean.class.getName()).log(Level.SEVERE, null, e);
		}
		
		
		contact.setCustomer(customer);

		
		return contact;
	}
	
	public void addContact(ContactBean localContact) {
		Contact contact = convertContactBeanToEntity(localContact);
		addContact(contact);	
	}

//	----------------------------------------------------------------
	
	public List<Contact> searchContactsByPositionAndPreference(String position, String preference) {
		List<Contact> output = new ArrayList<>();
		try {
            boolean contentCheck = false;
            for (Contact contact : contactRepository.getAllContacts()) {
                String position1 = contact.getPosition();
                String preference1 = contact.getPreference();
                if(!position.equals("") && !preference.equals("")) {
                	contentCheck = true;
                	if(position.equals(position1) && preference.equals(preference1))
                    	output.add(contact); 
                }else if(position.equals("") && !preference.equals("")) {
                	contentCheck = true;
                	if(preference.equals(preference1))
                		output.add(contact); 
                }else if(!position.equals("") && preference.equals("")) {
                	contentCheck = true;
                	if(position.equals(position1))
                		output.add(contact); 
                }                               	
            }            
//            if(!contentCheck) {
//            	FacesContext.getCurrentInstance().addMessage(component.getClientId(), new FacesMessage("NO INPUT, please select a industry and/or enter a year!!"));
//            }
        } catch (Exception ex) {
            Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }		
		return output;
	}
	
	public Set<String> getAllPosition(){
		Set<String> outputs = new HashSet<String>();
		try {
			List<Contact> contacts = contactRepository.getAllContacts();
			for (Contact contact : contacts) {
				String position = contact.getPosition();
				outputs.add(position);					
			}			
		} catch (Exception e) {
			Logger.getLogger(ContactManagedBean.class.getName()).log(Level.SEVERE, null, e);
		}
		return outputs;
	}
	

	
	
	
//	================================================================
	// FOR CON-DOMESTIC
	
	public List<ConDomestic> getAllConDomestics(){
		try {
			return contactRepository.getAllConDomestics();
		} catch (Exception e) {
			Logger.getLogger(ContactManagedBean.class.getName()).log(Level.SEVERE, null, e);
		}
		return null;
	}
	
	
	public void removeConDomestic(int contactId) {
		try {
			contactRepository.removeConDomestic(contactId);
		} catch (Exception e) {
			Logger.getLogger(ContactManagedBean.class.getName()).log(Level.SEVERE, null, e);
		}
	}
	
	public void editConDomestic(ConDomestic contact) {
		try {
			contactRepository.editConDomestic(contact);
			FacesContext.getCurrentInstance().addMessage(component.getClientId(), new FacesMessage("Domestic Contact Updated !!"));
		} catch (Exception e) {
			Logger.getLogger(ContactManagedBean.class.getName()).log(Level.SEVERE, null, e);
		}
	}
	
	private ConDomestic convertBeanToDomesticEntity(ContactBean localContact) {
//		Contact contact = new Contact();
		ConDomestic contact = new ConDomestic();
		
		String firstName = localContact.getFirstName();
		String lastName = localContact.getLastName();
		String position = localContact.getPosition();
		String email = localContact.getEmail();
		String phoneNo = localContact.getPhoneNo();
		String preference = localContact.getPreference();
		
		contact.setFirstName(firstName);
		contact.setLastName(lastName);
		contact.setPosition(position);
		contact.setEmail(email);
		contact.setPhoneNo(phoneNo);
		contact.setPreference(preference);
		
		contact.setState(localContact.getState());
		
		int customerId = localContact.getCustomerId();
		Customer customer = new Customer();
		try {
			for (Customer customerElement : contactRepository.getAllCustomers()) {
			    if (customerElement.getCustomerId() == customerId) {
			        customer = customerElement;
			    }
			}
		} catch (Exception e) {
			Logger.getLogger(ContactManagedBean.class.getName()).log(Level.SEVERE, null, e);
		}
		
		contact.setCustomer(customer);

		return contact;
	}
	
	public void addConDomestic(ContactBean localContact) {
		ConDomestic conDomestic = convertBeanToDomesticEntity(localContact);
		try {
			contactRepository.addConDomestic(conDomestic);
		} catch (Exception e) {
			Logger.getLogger(ContactManagedBean.class.getName()).log(Level.SEVERE, null, e);
		}		
	}

	
//	==============================================================
	// FOR CON-INTERNATIONAL
	
	public List<ConInternational> getAllConInternationals(){
		try {
			return contactRepository.getAllConInternationals();
		} catch (Exception e) {
			Logger.getLogger(ContactManagedBean.class.getName()).log(Level.SEVERE, null, e);
		}
		return null;
	}
	
	public void removeConInternational(int contactId) {
		try {
			contactRepository.removeConInternational(contactId);
		} catch (Exception e) {
			Logger.getLogger(ContactManagedBean.class.getName()).log(Level.SEVERE, null, e);
		}
	}
	
	public void editConInternational(ConInternational contact) {
		try {
			contactRepository.editConInternational(contact);
			FacesContext.getCurrentInstance().addMessage(component.getClientId(), new FacesMessage("International Contact Updated !!"));
		} catch (Exception e) {
			Logger.getLogger(ContactManagedBean.class.getName()).log(Level.SEVERE, null, e);
		}
	}
	
	private ConInternational convertBeanToInternationalEntity(ContactBean localContact) {
//		Contact contact = new Contact();
		ConInternational contact = new ConInternational();
		
		String firstName = localContact.getFirstName();
		String lastName = localContact.getLastName();
		String position = localContact.getPosition();
		String email = localContact.getEmail();
		String phoneNo = localContact.getPhoneNo();
		String preference = localContact.getPreference();
		
		contact.setFirstName(firstName);
		contact.setLastName(lastName);
		contact.setPosition(position);
		contact.setEmail(email);
		contact.setPhoneNo(phoneNo);
		contact.setPreference(preference);
		
		contact.setCountry(localContact.getCountry());
		
		int customerId = localContact.getCustomerId();
		Customer customer = new Customer();
		try {
			for (Customer customerElement : contactRepository.getAllCustomers()) {
			    if (customerElement.getCustomerId() == customerId) {
			        customer = customerElement;
			    }
			}
		} catch (Exception e) {
			Logger.getLogger(ContactManagedBean.class.getName()).log(Level.SEVERE, null, e);
		}
		
		contact.setCustomer(customer);

		return contact;
	}
	
	public void addConInternational(ContactBean localContact) {
		ConInternational conInternational = convertBeanToInternationalEntity(localContact);
		try {
			contactRepository.addConInternational(conInternational);
		} catch (Exception e) {
			Logger.getLogger(ContactManagedBean.class.getName()).log(Level.SEVERE, null, e);
		}	
	}
	
	
//	==========================================================
	// FOR WEB SERVICES (COUNT THE CONTACT)
	public int countContact() {
		List<Contact> contacts = getAllContacts();
		int count = contacts.size();
		return count;
	} 
	
}
