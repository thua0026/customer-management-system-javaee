package fit5042.assign.repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import fit5042.assign.repository.entities.Contact;
import fit5042.assign.repository.entities.Customer;
import fit5042.assign.repository.entities.Staff;

@Stateless
public class JPACustomerRepositoryImpl implements CustomerRepository{
	
	@PersistenceContext(unitName = "Assignment-ejbPU")
	private EntityManager entityManager;

	@Override
	public void addCustomer(Customer customer) throws Exception {
		// TODO Auto-generated method stub
		List<Customer> customers = entityManager.createNamedQuery(Customer.GET_ALL_QUERY_NAME).getResultList();
		
		int maxId = customers.get(customers.size()-1).getCustomerId();
		
		customer.setCustomerId(maxId+1);
		entityManager.persist(customer);
	}

	@Override
	public Customer searchCustomerById(int customerId) throws Exception {

//		Customer customer = entityManager.find(Customer.class, customerId);
		
		//https://www.objectdb.com/java/jpa/query/criteria
		// Create a Criteria Builder
    	CriteriaBuilder builder = entityManager.getCriteriaBuilder();
    	// Create a query
    	CriteriaQuery query = builder.createQuery(Customer.class);
    	//Create a root property
    	Root<Contact> c = query.from(Customer.class);
    	// Create and execute criteria query
    	query.select(c).where(builder.equal(c.get("customerId"), customerId));
    	// return the result
    	return (Customer) entityManager.createQuery(query).getResultList().get(0);
		
		//customer.getTags();
//		return customer;
	}

	@Override
	public List<Customer> getAllCustomers() throws Exception {
		// TODO Auto-generated method stub
		List<Customer> customers = entityManager.createNamedQuery(Customer.GET_ALL_QUERY_NAME).getResultList();
		return customers;
	}

	@Override
	public void removeCustomer(int customerId) throws Exception {
		// TODO Auto-generated method stub
		Customer customer = searchCustomerById(customerId);
		if(customer != null)
			entityManager.remove(customer);		
	}

	@Override
	public void editCustomer(Customer customer) throws Exception {
		// TODO Auto-generated method stub
		try {
			entityManager.merge(customer);
		} catch (Exception e) {
			
		}
	}

	@Override
	public List<Contact> getAllContacts() throws Exception{
		return entityManager.createNamedQuery(Contact.GET_ALL_QUERY_NAME).getResultList();
	}

	@Override
	public Customer searchCustomerByContact(Contact contact) throws Exception {
		contact = entityManager.find(Contact.class, contact.getContactId());
        contact.getCustomer();
        entityManager.refresh(contact);

        return contact.getCustomer();
	}

//	@Override
//	public List<Customer> searchCustomersByYearAndIndustry(int yearRegistered, int industryId) throws Exception {
//		List<Customer> customers = entityManager.createNamedQuery(Customer.GET_ALL_QUERY_NAME).getResultList();
//		List<Customer> output = new ArrayList<Customer>();
//		for(Customer customer: customers) {
//			if(customer.getYearRegistered()==yearRegistered)
//				output.add(customer);
//		}
//		return output;
//	}

}
