package fit5042.assign.repository;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import fit5042.assign.repository.entities.Customer;
import fit5042.assign.repository.entities.Industry;

@Stateless
public class JPAIndustryRepositoryImpl implements IndustryRepository{

	@PersistenceContext(unitName = "Assignment-ejbPU")
	private EntityManager entityManager;
	
	@Override
	public void addIndustry(Industry industry) throws Exception {
		// TODO Auto-generated method stub
		List<Industry> industries = entityManager.createNamedQuery(Industry.GET_ALL_QUERY_NAME).getResultList();
		
		int maxId = industries.get(industries.size()-1).getIndustryId();
		
		industry.setIndustryId(maxId+1);
		entityManager.persist(industry);
	}

	@Override
	public Industry searchIndustryById(int industryId) throws Exception {
		Industry industry = entityManager.find(Industry.class, industryId);
		//customer.getTags();
		return industry;
	}

	@Override
	public List<Industry> getAllIndustries() throws Exception {
		// TODO Auto-generated method stub
		List<Industry> industries = entityManager.createNamedQuery(Industry.GET_ALL_QUERY_NAME).getResultList();
		return industries;
	}

	@Override
	public void removeIndustry(int industryId) throws Exception {
		// TODO Auto-generated method stub
		Industry industry = searchIndustryById(industryId);
		if(industry != null)
			entityManager.remove(industry);	
	}

	@Override
	public void editIndustry(Industry industry) throws Exception {
		// TODO Auto-generated method stub
		try {
			entityManager.merge(industry);
		} catch (Exception e) {
			
		}
	}

	
}
