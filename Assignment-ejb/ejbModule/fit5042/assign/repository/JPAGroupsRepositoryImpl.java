package fit5042.assign.repository;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import fit5042.assign.repository.entities.Groups;

@Stateless
public class JPAGroupsRepositoryImpl implements GroupsRepository{
	
	@PersistenceContext(unitName = "Assignment-ejbPU")
	private EntityManager entityManager;

	@Override
	public void addGroups(Groups groups) throws Exception {
		// TODO Auto-generated method stub
		List<Groups> groups1 = entityManager.createNamedQuery(Groups.GET_ALL_QUERY_NAME).getResultList();
		
		int maxId = groups1.get(groups1.size()-1).getId();
		
		groups.setId(maxId+1);
		entityManager.persist(groups);
	}

	@Override
	public Groups searchGroupsById(int id) throws Exception {
		// TODO Auto-generated method stub
		Groups groups = entityManager.find(Groups.class, id);
		return groups;
	}

	@Override
	public List<Groups> getAllGroups() throws Exception {
		// TODO Auto-generated method stub
		List<Groups> groups1 = entityManager.createNamedQuery(Groups.GET_ALL_QUERY_NAME).getResultList();
		return groups1;
	}

	@Override
	public void removeGroups(int id) throws Exception {
		// TODO Auto-generated method stub
		Groups groups = searchGroupsById(id);
		if(groups!=null)
			entityManager.remove(groups);
		
	}

	@Override
	public void editGroups(Groups groups) throws Exception {
		// TODO Auto-generated method stub
		try {
			entityManager.merge(groups);
		}catch (Exception e) {

		}
		
	}
	
	
	
}
