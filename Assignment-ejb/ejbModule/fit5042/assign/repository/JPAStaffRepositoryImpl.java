package fit5042.assign.repository;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import fit5042.assign.repository.entities.Contact;
import fit5042.assign.repository.entities.Staff;

@Stateless
public class JPAStaffRepositoryImpl implements StaffRepository{

	@PersistenceContext(unitName = "Assignment-ejbPU")
	private EntityManager entityManager;

	@Override
	public void addStaff(Staff staff) throws Exception {
		// TODO Auto-generated method stub
		List<Staff> staffs = entityManager.createNamedQuery(Staff.GET_ALL_QUERY_NAME).getResultList();
		int maxId = staffs.get(staffs.size()-1).getId();
		staff.setId(maxId+1);
		entityManager.persist(staff);
	}

	@Override
	public Staff searchStaffById(int id) throws Exception {
		// TODO Auto-generated method stub
		Staff staff = entityManager.find(Staff.class, id);
		return staff;
	}

	@Override
	public List<Staff> getAllStaffs() throws Exception {
		// TODO Auto-generated method stub
		List<Staff> staffs = entityManager.createNamedQuery(Staff.GET_ALL_QUERY_NAME).getResultList();
		return staffs;
	}

	@Override
	public void removeStaff(int id) throws Exception {
		// TODO Auto-generated method stub
		Staff staff = searchStaffById(id);
		if(staff != null)
			entityManager.remove(staff);	
	}

	@Override
	public void editStaff(Staff staff) throws Exception {
		// TODO Auto-generated method stub
		try {
			entityManager.merge(staff);
		} catch (Exception e) {
			
		}
	}
	
}
