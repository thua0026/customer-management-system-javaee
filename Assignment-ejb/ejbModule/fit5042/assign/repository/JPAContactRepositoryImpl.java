package fit5042.assign.repository;

import java.util.List;
import java.util.Set;
import java.util.function.Predicate;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Root;

import com.sun.tools.xjc.model.CBuiltinLeafInfo;

import fit5042.assign.repository.entities.ConDomestic;
import fit5042.assign.repository.entities.ConInternational;
import fit5042.assign.repository.entities.Contact;
import fit5042.assign.repository.entities.Customer;

@Stateless
public class JPAContactRepositoryImpl implements ContactRepository{

	@PersistenceContext(unitName = "Assignment-ejbPU")
	private EntityManager entityManager;

	@Override
	public void addContact(Contact contact) throws Exception {
		// TODO Auto-generated method stub
		List<Contact> contacts = entityManager.createNamedQuery(Contact.GET_ALL_QUERY_NAME).getResultList();

		int maxId = contacts.get(contacts.size()-1).getContactId();
		
		contact.setContactId(maxId+1);

		entityManager.persist(contact);
	}

	@Override
	public Contact searchContactById(int contactId) throws Exception {

//		Contact contact = entityManager.find(Contact.class, contactId);
		
		//https://www.objectdb.com/java/jpa/query/criteria
		// Create a Criteria Builder
    	CriteriaBuilder builder = entityManager.getCriteriaBuilder();
    	// Create a query
    	CriteriaQuery query = builder.createQuery(Contact.class);
    	//Create a root property
    	Root<Contact> c = query.from(Contact.class);
//    	//Construct the criteria for filtering entity instance
//    	ParameterExpression<Integer> p = builder.parameter(Integer.class);
    	// Create and execute criteria query
    	query.select(c).where(builder.equal(c.get("contactId"), contactId));
		// return the result
    	return (Contact) entityManager.createQuery(query).getResultList().get(0);
    	
//		return contact;
	}

	@Override
	public List<Contact> getAllContacts() throws Exception {
		// TODO Auto-generated method stub
		List<Contact> contacts = entityManager.createNamedQuery(Contact.GET_ALL_QUERY_NAME).getResultList();
		return contacts;
	}

	@Override
	public void removeContact(int contactId) throws Exception {
		// TODO Auto-generated method stub
		Contact contact = searchContactById(contactId);
		if(contact != null)
			entityManager.remove(contact);	
	}

	@Override
	public void editContact(Contact contact) throws Exception {
		// TODO Auto-generated method stub
		try {
			entityManager.merge(contact);
		} catch (Exception e) {
			
		}
	}

	@Override
	public List<Customer> getAllCustomers() throws Exception{
		List<Customer> customers = entityManager.createNamedQuery(Customer.GET_ALL_QUERY_NAME).getResultList();
		return customers;
	}

	@Override
	public Set<Contact> searchContactByCustomer(Customer customer) throws Exception{
		customer = entityManager.find(Customer.class, customer.getCustomerId());
        customer.getContacts().size();
        entityManager.refresh(customer);

    	return customer.getContacts();
	}


	
//	========================================================
	
	@Override
	public void addConDomestic(ConDomestic conDomestic) throws Exception {

//		List<ConDomestic> conDomestics = entityManager.createNamedQuery(ConDomestic.GET_ALL_QUERY_NAME).getResultList();
//		int maxId = conDomestics.get(conDomestics.size()-1).getContactId();
		
		List<Contact> contacts = entityManager.createNamedQuery(Contact.GET_ALL_QUERY_NAME).getResultList();
		int maxId = contacts.get(contacts.size()-1).getContactId();
		
		conDomestic.setContactId(maxId+1);

		entityManager.persist(conDomestic);
	}

	@Override
	public ConDomestic searchConDomesticById(int contactId) throws Exception {
		// Create a Criteria Builder
    	CriteriaBuilder builder = entityManager.getCriteriaBuilder();
    	// Create a query
    	CriteriaQuery query = builder.createQuery(ConDomestic.class);
    	//Create a root property
    	Root<ConDomestic> c = query.from(ConDomestic.class);
//    	//Construct the criteria for filtering entity instance
//    	ParameterExpression<Integer> p = builder.parameter(Integer.class);
    	// Create and execute criteria query
    	query.select(c).where(builder.equal(c.get("contactId"), contactId));
		// return the result
    	return (ConDomestic) entityManager.createQuery(query).getResultList().get(0);
	}

	@Override
	public List<ConDomestic> getAllConDomestics() throws Exception {
		List<ConDomestic> conDomestics = entityManager.createNamedQuery(ConDomestic.GET_ALL_CONDOMESTIC).getResultList();
		return conDomestics;
	}

	@Override
	public void removeConDomestic(int contactId) throws Exception {

		ConDomestic conDomestic = searchConDomesticById(contactId);
		if(conDomestic != null)
			entityManager.remove(conDomestic);	
	}

	@Override
	public void editConDomestic(ConDomestic conDomestic) throws Exception {
		try {
			entityManager.merge(conDomestic);
		} catch (Exception e) {
			
		}
	}

	
//	========================================================
	
	@Override
	public void addConInternational(ConInternational conInternational) throws Exception {
//		List<ConInternational> conInternationals = entityManager.createNamedQuery(ConInternational.GET_ALL_QUERY_NAME).getResultList();
//		int maxId = conInternationals.get(conInternationals.size()-1).getContactId();
		
		List<Contact> contacts = entityManager.createNamedQuery(Contact.GET_ALL_QUERY_NAME).getResultList();
		int maxId = contacts.get(contacts.size()-1).getContactId();		
		
		conInternational.setContactId(maxId+1);

		entityManager.persist(conInternational);
		
	}

	@Override
	public ConInternational searchConInternationalById(int contactId) throws Exception {

		// Create a Criteria Builder
    	CriteriaBuilder builder = entityManager.getCriteriaBuilder();
    	// Create a query
    	CriteriaQuery query = builder.createQuery(ConInternational.class);
    	//Create a root property
    	Root<ConInternational> c = query.from(ConInternational.class);
//    	//Construct the criteria for filtering entity instance
//    	ParameterExpression<Integer> p = builder.parameter(Integer.class);
    	// Create and execute criteria query
    	query.select(c).where(builder.equal(c.get("contactId"), contactId));
		// return the result
    	return (ConInternational) entityManager.createQuery(query).getResultList().get(0);
	}

	@Override
	public List<ConInternational> getAllConInternationals() throws Exception {

		List<ConInternational> conInternationals = entityManager.createNamedQuery(ConInternational.GET_ALL_CONINTERNATIONAL).getResultList();
		return conInternationals;
	}

	@Override
	public void removeConInternational(int contactId) throws Exception {

		ConInternational conInternational = searchConInternationalById(contactId);
		if(conInternational != null)
			entityManager.remove(conInternational);	
	}

	@Override
	public void editConInternational(ConInternational conInternational) throws Exception {
		try {
			entityManager.merge(conInternational);
		} catch (Exception e) {
			
		}
		
	}
	
	
	
}
